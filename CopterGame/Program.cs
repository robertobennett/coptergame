﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace CopterGame
{
    //This class holds the entry point to the application and controls the low level features such as frame rate
    class Program
    {
        //need a stopwatch to control the framerate
        static Stopwatch m_GameTimer = new Stopwatch();
        //This class will manage the game; it will control the states in the game and handle messages
        static CGame m_Game = CGame.Instance;
        //this is the target frame rate of the game
        static int m_iTargetFPS = 60;
        //this is the entrypoint to the application
        static void Main(string[] args)
        {
            //start the stopwatch that will control the framerate
            m_GameTimer.Start();
            //intialize the game
            m_Game.Initialize();
            //start the key manager listening - this will create the listening thread
            CKeyManager.Instance.StartListening();
            //store the elapsed milliseconds since the timer was started to calculate the time between frames
            long lLastTime = 0; //it needs to start zero so the first frame is called instantly when the game starts playing
            //store the last message recieved - initialized to no messages
            CGame.GameMessages msg = CGame.GameMessages.NO_MESSAGES;
            //enter the game loop until the exit message is posted
            while (msg != CGame.GameMessages.EXIT)
            {
                //grab the latest message
                msg = m_Game.PeekMessage(true);

                //if there is a message process it and do not process a frame
                if (msg != CGame.GameMessages.NO_MESSAGES)
                {
                    //dispatch the message to all listeners
                    m_Game.DispatchMessage(msg);
                }
                //if the time between last update and current time is greater then a thirtieth of a second update the game; also only update if the game is running
                else if (m_Game.State == CGame.GameStates.GAME_RUNNING && (m_GameTimer.ElapsedMilliseconds - lLastTime) >= (long)(((double)(1.0d / m_iTargetFPS) * 1000.0d) - 0.5d))
                {
                    //update
                    m_Game.Update();
                    //update the last time
                    lLastTime = m_GameTimer.ElapsedMilliseconds;
                }
            }

            //Exit message was peeked, so shutdown the game
            m_Game.Shutdown();
            //Stop the timer
            m_GameTimer.Stop();
            //reset the timer
            m_GameTimer.Reset();
            //stop listening this will kill the thread and allow the program to close
            CKeyManager.Instance.StopListening();
            return;
        }
    }
}