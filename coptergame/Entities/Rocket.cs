﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using CopterGame.Interfaces;

namespace CopterGame.Entities
{
    //this is a rocket class, it is spawned at a position and given a target it will then travel to its target
    class CRocket : CEntity
    {
        //all instances of the box class can share one instance of the geometry so this saves alot of memory and performance
        private static CGeometry sm_RocketGeom = new CGeometry(CGeometry.GetGeomDir() + "Rocket.geom");
        //a way to access the width of the rocket without an instance of this classs
        public static int GeomWidth
        {
            get
            {
                return sm_RocketGeom.Dimensions.X;
            }
        }
        //a vector to store the target position
        private Math.Vector2D m_TargetPos = new Math.Vector2D(0, 0);
        //protperty to get and set the target pos
        public Math.Vector2D TargetPos
        {
            get
            {
                return m_TargetPos;
            }

            set
            {
                m_TargetPos = value;
            }
        }
        //public constructor as it will be called by the entity manager
        public CRocket()
        {
            //set the geometry variable (inhertied from CEntity) to that static instance of the geometry
            this.m_Geometry = sm_RocketGeom;
            //set that bDraw flag to true as we want it to be drawn
            this.m_bDraw = true;
            //set the initial 2dPos this will almost certainly not be the actual position on creation, initializing to this as the object wont intstantly be destroyed
            this.m_2dPos = new Math.Vector2D(CCanvas.Instance.DrawXStart, CCanvas.Instance.DrawYStart);
            //intialize the previous pos to the start pos
            this.m_2dPrevPos = this.m_2dPos;
            //set the dimensions to match that in the geometry
            this.m_2dDimensions = this.m_Geometry.Dimensions;
            //this is a projectile so set the IsProjectile flag to true
            this.m_bIsProjectile = true;

            return;
        }
        //called per frame
        public override void Update()
        {
            //update previous pos
            this.m_2dPrevPos = m_2dPos;
            //move three spaces right
            m_2dPos = m_2dPos.Add(new Math.Vector2D(-3, 0));
            //calculate vector to get from current pos to target pos
            Math.Vector2D posToTarg = m_TargetPos.Subtract(m_2dPos);
            //if the pos to target vector y component is less then 0 then we need the rocket to move up
            if(posToTarg.Y < 0)
            {
                //move the rocket one unit up
                m_2dPos = m_2dPos.Add(new Math.Vector2D(0, -1));
            }
            else if (posToTarg.Y > 0)
            {
                //other wise if its greater then 0 move the rocket down one unit
                m_2dPos = m_2dPos.Add(new Math.Vector2D(0, 1));
            }

            //if the alive flag is false
            if (!m_bIsAlive)
            {
                //then check if it should score points (only if the rocket made it to the end) and add 2 points
                if(m_bScorePoints)
                    CGame.Instance.AddScore(2);

                //then remove this entity
                CEntityManager.Instance.DestroyEntity(this);
            }

            return;
        }
        //called when the rocket collides with an objcet
        public override void CollisionEvent(IPhysicalable obj)
        {
            //if it died due to a collision then we arent scoreing points
            m_bScorePoints = false;
            //set alive flag to false
            m_bIsAlive = false;
        }
    }
}