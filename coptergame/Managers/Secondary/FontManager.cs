﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Managers
{
    //a class to convert strings to ascii art, using singleton pattern as only need one instance
    class CFontManager
    {
        //store statically as they will always be the same even if more then one instance was created
        private static CGeometry m_ScoreText = new CGeometry(CGeometry.GetGeomDir() + "Score.font");
        private static CGeometry m_One = new CGeometry(CGeometry.GetGeomDir() + "One.font");
        private static CGeometry m_Two = new CGeometry(CGeometry.GetGeomDir() + "Two.font");
        private static CGeometry m_Three = new CGeometry(CGeometry.GetGeomDir() + "Three.font");
        private static CGeometry m_Four = new CGeometry(CGeometry.GetGeomDir() + "Four.font");
        private static CGeometry m_Five = new CGeometry(CGeometry.GetGeomDir() + "Five.font");
        private static CGeometry m_Six = new CGeometry(CGeometry.GetGeomDir() + "Six.font");
        private static CGeometry m_Seven = new CGeometry(CGeometry.GetGeomDir() + "Seven.font");
        private static CGeometry m_Eight = new CGeometry(CGeometry.GetGeomDir() + "Eight.font");
        private static CGeometry m_Nine = new CGeometry(CGeometry.GetGeomDir() + "Nine.font");
        private static CGeometry m_Zero = new CGeometry(CGeometry.GetGeomDir() + "Zero.font");
        private static CGeometry m_A = new CGeometry(CGeometry.GetGeomDir() + "A.font");
        private static CGeometry m_B = new CGeometry(CGeometry.GetGeomDir() + "B.font");
        private static CGeometry m_C = new CGeometry(CGeometry.GetGeomDir() + "C.font");
        private static CGeometry m_D = new CGeometry(CGeometry.GetGeomDir() + "D.font");
        private static CGeometry m_E = new CGeometry(CGeometry.GetGeomDir() + "E.font");
        private static CGeometry m_F = new CGeometry(CGeometry.GetGeomDir() + "F.font");
        private static CGeometry m_G = new CGeometry(CGeometry.GetGeomDir() + "G.font");
        private static CGeometry m_H = new CGeometry(CGeometry.GetGeomDir() + "H.font");
        private static CGeometry m_I = new CGeometry(CGeometry.GetGeomDir() + "I.font");
        private static CGeometry m_J = new CGeometry(CGeometry.GetGeomDir() + "J.font");
        private static CGeometry m_K = new CGeometry(CGeometry.GetGeomDir() + "K.font");
        private static CGeometry m_L = new CGeometry(CGeometry.GetGeomDir() + "L.font");
        private static CGeometry m_M = new CGeometry(CGeometry.GetGeomDir() + "M.font");
        private static CGeometry m_N = new CGeometry(CGeometry.GetGeomDir() + "N.font");
        private static CGeometry m_O = new CGeometry(CGeometry.GetGeomDir() + "O.font");
        private static CGeometry m_P = new CGeometry(CGeometry.GetGeomDir() + "P.font");
        private static CGeometry m_Q = new CGeometry(CGeometry.GetGeomDir() + "Q.font");
        private static CGeometry m_R = new CGeometry(CGeometry.GetGeomDir() + "R.font");
        private static CGeometry m_S = new CGeometry(CGeometry.GetGeomDir() + "S.font");
        private static CGeometry m_T = new CGeometry(CGeometry.GetGeomDir() + "T.font");
        private static CGeometry m_U = new CGeometry(CGeometry.GetGeomDir() + "U.font");
        private static CGeometry m_V = new CGeometry(CGeometry.GetGeomDir() + "V.font");
        private static CGeometry m_W = new CGeometry(CGeometry.GetGeomDir() + "W.font");
        private static CGeometry m_X = new CGeometry(CGeometry.GetGeomDir() + "X.font");
        private static CGeometry m_Y = new CGeometry(CGeometry.GetGeomDir() + "Y.font");
        private static CGeometry m_Z = new CGeometry(CGeometry.GetGeomDir() + "Z.font");
        private static CGeometry m_Space = new CGeometry(CGeometry.GetGeomDir() + "Space.font");
        //hide constructor as singleton
        private CFontManager()
        {
            return;
        }
        //store an instance of the class privatly
        private static CFontManager sm_Instance = null;
        //create a property to get the instance of the class
        public static CFontManager Instance
        {
            get
            {
                //if the instance is null create an instance
                if (sm_Instance == null)
                    sm_Instance = new CFontManager();

                //return the instance
                return sm_Instance;
            }
        }
        //utillitiy function to convert a string to a list of geometries
        public List<CGeometry> StringToGeom(String s, ref int width)
        {
            //create a list of geoms to be returned
            List<CGeometry> geoms = new List<CGeometry>();
            //for the lenght of the string
            for (int i = 0; i < s.Length; i++)
            {
                //convert each iterated character to geometry
                geoms.Add(CharToGeom(s[i]));
                //update the passed by reference width with the width of each converted character
                width += geoms[i].Dimensions.X;
            }
            //return the list of geoms
            return geoms;
        }

        public CGeometry CharToGeom(char c)
        {
            //convert the char to upper case
            String s = "";
            s += c;
            s = s.ToUpper();
            //switch statement to retrive relevant ascii art for the char
            switch (s[0])
            {
                //throw exception as the font item is not supported
                default:
                    throw new Exception("Cannot convert char (code (decimal): " + (int)c + ") to geom");

                case 'A':
                    return m_A;

                case 'B':
                    return m_B;

                case 'C':
                    return m_C;

                case 'D':
                    return m_D;

                case 'E':
                    return m_E;

                case 'F':
                    return m_F;

                case 'G':
                    return m_G;

                case 'H':
                    return m_H;

                case 'I':
                    return m_I;

                case 'J':
                    return m_J;

                case 'K':
                    return m_K;

                case 'L':
                    return m_L;

                case 'M':
                    return m_M;

                case 'N':
                    return m_N;

                case 'O':
                    return m_O;

                case 'P':
                    return m_P;

                case 'Q':
                    return m_Q;

                case 'R':
                    return m_R;

                case 'S':
                    return m_S;

                case 'T':
                    return m_T;

                case 'U':
                    return m_U;

                case 'V':
                    return m_V;

                case 'W':
                    return m_W;

                case 'X':
                    return m_X;

                case 'Y':
                    return m_Y;

                case 'Z':
                    return m_Z;

                case ' ':
                    return m_Space;

                case '0':
                    return m_Zero;

                case '1':
                    return m_One;

                case '2':
                    return m_Two;

                case '3':
                    return m_Three;

                case '4':
                    return m_Four;

                case '5':
                    return m_Five;

                case '6':
                    return m_Six;

                case '7':
                    return m_Seven;

                case '8':
                    return m_Eight;

                case '9':
                    return m_Nine;
            }
        }
    }
}
