﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame
{
    //this class is responsible for keeping track of all the entites and creating them, updating them and disposing of them, using a singleton as I only need one instance
    class CEntityManager
    {
        //store the instance as the object privatly
        private static CEntityManager sm_Instance = null;
        //hide constructor as it is a singleton
        private CEntityManager()
        {
            return;
        }
        //create a property to get the instance of the class
        public static CEntityManager Instance
        {
            get
            {
                //if the instance is not created create it
                if(sm_Instance == null)
                    sm_Instance = new CEntityManager();

                //return the instance
                return sm_Instance;
            }
        }
        //a list to store all the entities
        private List<Entities.CEntity> m_Entities = new List<Entities.CEntity>();
        //use a generic so that this function can be called to create an entity, this also ensures that the constructor is called and the instance of the actual object is returned, T is required to derrive from CEntity and have access to the constructor
        public T CreateEntity<T>() where T : Entities.CEntity, new()
        {
            //create the entity instance
            T ent = new T();

            //register it with physics and renderer
            CRenderer.Instance.RegisterRenderable(ent);
            CPhysics.Instance.RegisterPhysicalable(ent);

            //add it to the entity list as base class
            m_Entities.Add(ent as Entities.CEntity);
            //return the instance
            return ent;
        }
        //this will remove and destroy the entity
        public void DestroyEntity<T>(T ent) where T : Entities.CEntity, new()
        {
            //search all the instances of entites stored for the entity truong to be destroyed
            for (int i = 0; i < m_Entities.Count; i++)
            {
                //if it is found to be that intance, there can only be one instance as it is uniquely created then added via CEntityManager.CreateEntity
                if (m_Entities[i] == ent as Entities.CEntity)
                {
                    //destroy it
                    m_Entities[i].Destroy();

                    //unregister it with physics and renderer
                    CRenderer.Instance.UnregisterRenderable(m_Entities[i]);
                    CPhysics.Instance.UnregisterPhysicalable(m_Entities[i]);
                    //remove it from the entity list
                    m_Entities.RemoveAt(i);

                    return;
                }
            }

            //it shouldnt be possible for this to be triggered if this class is being used correctly so we want to throw an exception
            throw new Exception("ENTITY NOT FOUND");
        }
        //this is a utility function to convert a interface instance of an object such as IRenderable or IPhysicable to the relevant entity
        public Entities.CEntity GetEntity<T>(T ent) where T : class
        {
            //check if passed ent is null
            if(ent != null)
            {
                //search the list of entities
                for(int i = 0; i < m_Entities.Count; i++)
                {
                    //check if the iterated entity cast as the type T is the instance passed if it is then return it
                    if(m_Entities[i] as T == ent)
                    {
                        return m_Entities[i];
                    }
                }
            }

            return null;
        }
        //utility function to quickly destryo all of the entities, start from the back of the list so removing the entity doesnt cause an out of bounds exception on the next iteration
        public void DestroyAll()
        {
            for(int i = (m_Entities.Count - 1); i >= 0 ; i--)
            {
                m_Entities[i].Destroy();

                CRenderer.Instance.UnregisterRenderable(m_Entities[i]);
                CPhysics.Instance.UnregisterPhysicalable(m_Entities[i]);

                m_Entities.RemoveAt(i);
            }
        }
        //called each frame, just goes through the list and calls the update function for each individual entity
        public void Update()
        {
            for (int i = 0; i < m_Entities.Count; i++)
                m_Entities[i].Update();

            return;
        }
    }
}
