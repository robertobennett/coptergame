﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame
{
    //this class outlines the menu item and its behaviour it will be used by main menu to
    class CMenuItem
    {
        //a boolean to store if the menu item is enabled
        private bool m_bEnabled = false;
        //property to set and get if its enabled
        public bool Enabled
        {
            get
            {
                return m_bEnabled;
            }
            set
            {
                //update the value of enabled
                m_bEnabled = value;
                //update the color of the item based on its states
                if(value == true && m_bSelected)
                {
                    //if the item is enabled and selected then it needs to be the selected color
                    m_CurrentColor = m_SelectedColor;
                }
                else if(value == true)
                {
                    //if the item is just enabled it needs to be the unselected color
                    m_CurrentColor = m_UnselectedColor;
                }
                else
                {
                    //otherwise if the item is disabled it needs to be the disabled color
                    m_CurrentColor = m_DisabledColor;
                }
                Redraw();
            }
        }
        //a boolean to tell whether or not it is selected
        private bool m_bSelected = false;
        //contents of the menu item
        private string m_sOldContents = "DEFAULT";

        //cant explain this other then in the terms of the C++ equivilant; basically stores the memory address of a function so that it can be passed around but called like the original
        //this is creating a type which can be used by the functions
        public delegate void voidfuncptr();
        //create a "function pointer" to store the action to call when enter is pressed 
        public voidfuncptr Action = null;
        //property to return wether or not the item is selected and also set wether it is selected
        public bool Selected
        {
            get
            {
                return m_bSelected;
            }

            set
            {
                //update the value of selected
                m_bSelected = value;
                //update the color of the item based on its states
                if (value == true && m_bEnabled)
                {
                    //if the item is enabled and selected then it needs to be the selected color
                    m_CurrentColor = m_SelectedColor;
                }
                else if (m_bEnabled)
                {
                    //if the item is just enabled it needs to be the unselected color
                    m_CurrentColor = m_UnselectedColor;
                }
                else
                {
                    //otherwise if the item is disabled it needs to be the disabled color
                    m_CurrentColor = m_DisabledColor;
                }
            }
        }
        //define the colors for selected, unselected and disabled
        private const ConsoleColor m_SelectedColor = ConsoleColor.Green;
        private const ConsoleColor m_UnselectedColor = ConsoleColor.White;
        private const ConsoleColor m_DisabledColor = ConsoleColor.Red;
        //store the current color
        private ConsoleColor m_CurrentColor = m_DisabledColor;
        //property to return the current color
        public ConsoleColor Color
        {
            get
            {
                return m_CurrentColor;
            }
        }
        //store the contents of the item
        private String m_sContents = "DEFAULT";
        //property to return or set the contents
        public String Contents
        {
            get
            {
                return m_sContents;
            }
            set
            {
                //need to store oldcontents so it is correctly erased
                m_sOldContents = m_sContents;
                //set the ccurrent contents to the value passed in
                m_sContents = value;
                //redraw the changes
                Redraw();
            }
        }
        //store x and y position
        private int m_iX, m_iY;
        //property to return the x value
        public int X
        {
            get
            {
                return m_iX;
            }
        }
        //property to return the y value
        public int Y
        {
            get
            {
                return m_iY;
            }
        }
        //store width and height of the menu items
        private int m_iWidth, m_iHeight;
        //property to return the width of menu item
        public int Width
        {
            get
            {
                return m_iWidth;
            }
        }
        //property to return the height of menu item
        public int Height
        {
            get
            {
                return m_iHeight;
            }
        }
        //constructor want to take in the x and y pos the contents and wether the item should be enabled or not 
        public CMenuItem(int iX, int iY, String sContents, bool bEnable = false)
        {
            //update the member vars with the passed in values
            m_bEnabled = bEnable;
            m_sContents = sContents;
            m_sOldContents = sContents;
            m_iX = iX;
            m_iY = iY;

            //set the intial color values
            if(m_bEnabled && m_bSelected)
            {
                m_CurrentColor = m_SelectedColor;
            }
            else if (m_bEnabled)
            {
                m_CurrentColor = m_UnselectedColor;
            }
            else
            {
                m_CurrentColor = m_DisabledColor;
            }

            m_iWidth = (int)((CCanvas.Instance.Width / 3) + 0.5f);
            //allows a maximum of 5 items  the + .5 ensures it always rounds up
            m_iHeight = (int)((CCanvas.Instance.Height / 5) + 0.5f);

            return;
        }
        //function to clear the rendered menu item
        public void Clear()
        {
            //from the x position iterate until the x pos + the width and set that "console square" to blank
            for (int x = m_iX; x < (m_iX + m_iWidth); x++)
            {
                //set the iterate x pos and the top row to blank
                Console.SetCursorPosition(x, m_iY);
                Console.Write(" ");
            }
            //from the x position iterate until the x pos + the width and set that "console square" to blank
            //+ 1 is for the same bug as drawwing
            for (int x = m_iX; x < (m_iX + m_iWidth + 1); x++)
            {
                //set the iterate x pos and the bottom row to blank
                Console.SetCursorPosition(x, m_iY + m_iHeight);
                Console.Write(" ");
            }
            //from the y pos to the y pos + the height
            for (int y = m_iY; y < (m_iY + m_iHeight); y++)
            {
                //set cursor x pos to the first column and the y cursor pos to the iterated y pos
                Console.SetCursorPosition(m_iX, y);
                Console.Write(" ");
            }

            for (int y = m_iY; y < (m_iY + m_iHeight); y++)
            {
                //set cursor x pos to the second column and the y cursor pos to the iterated y pos
                Console.SetCursorPosition(m_iX + m_iWidth, y);
                Console.Write(" ");
            }

            //work out the text positions
            int textX = m_iX + (int)((m_iWidth - m_sOldContents.Count()) / 2);
            int textY = m_iY + (int)(m_iHeight / 2);

            //set the cursor pos to the text position
            Console.SetCursorPosition(textX, textY);
            //for everyletter in the contents set to blank
            for (int i = 0; i < m_sContents.Count(); i++)
                Console.Write(" ");
        }

        //use Console functions rather than my canvas class as I need access to the colors
        public void Redraw()
        {
            //set the console text color to the current color of menu item
            Console.ForegroundColor = m_CurrentColor;
            //clear the console of current menu item
            Clear();
            //draw the rows
            //from the x position iterate until the x pos + the width and set that "console square" to -
            for (int x = m_iX; x < (m_iX + m_iWidth); x++)
            {
                Console.SetCursorPosition(x, m_iY);
                Console.Write("-");
            }
            //from the x position iterate until the x pos + the width and set that "console square" to -
            //plus one is an offset. not sure why but strange bug where - would be missing
            for (int x = m_iX; x < (m_iX + m_iWidth + 1); x++)
            {
                Console.SetCursorPosition(x, m_iY + m_iHeight);
                Console.Write("-");
            }
            //now draw the columns
            //so iteate through y positions for first column and then draw -
            for (int y = m_iY; y < (m_iY + m_iHeight); y++)
            {
                Console.SetCursorPosition(m_iX, y);
                Console.Write("-");
            }
            //so iteate through y positions for second columns and then draw -
            for (int y = m_iY; y < (m_iY + m_iHeight); y++)
            {
                Console.SetCursorPosition(m_iX + m_iWidth, y);
                Console.Write("-");
            }
            //now work out the text position and draw the text
            int textX = m_iX + (int)((m_iWidth - m_sContents.Count()) / 2);
            int textY = m_iY + (int)(m_iHeight / 2);
            Console.SetCursorPosition(textX, textY);
            Console.Write(m_sContents);

            Console.ForegroundColor = m_UnselectedColor;
        }
    }
}