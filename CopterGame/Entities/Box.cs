﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CopterGame.Interfaces;

namespace CopterGame.Entities
{
    //this is the basic obstacle class which is used, it is simply a box the player must avoid
    class CBox : CEntity
    {
        //all instances of the box class can share one instance of the geometry so this saves alot of memory and performance
        private static CGeometry sm_BoxGeom = new CGeometry(CGeometry.GetGeomDir() + "Box.geom");
        //public constructor as it will be called by the entity manager
        public CBox()
        {
            //set the geometry variable (inhertied from CEntity) to that static instance of the geometry
            this.m_Geometry = sm_BoxGeom;
            //set that bDraw flag to true as we want it to be drawn
            this.m_bDraw = true;
            //set the initial 2dPos this will almost certainly not be the actual position on creation, initializing to this as the object wont intstantly be destroyed
            this.m_2dPos = new Math.Vector2D(CCanvas.Instance.DrawXStart, CCanvas.Instance.DrawYStart);
            //intialize the previous pos to the start pos
            this.m_2dPrevPos = this.m_2dPos;
            //set the dimensions to match that in the geometry
            this.m_2dDimensions = this.m_Geometry.Dimensions;

            return;
        }
        //override the CEntity.Update() function
        public override void Update()
        {
            //update the previous pos
            this.m_2dPrevPos = m_2dPos;
            //update the new pos
            this.m_2dPos = this.m_2dPos.Add(new Math.Vector2D(-1, 0));

            //if the object isnt alive
            if (!m_bIsAlive)
            {
                //check if the score flag is set to true if it is then add the score
                if(m_bScorePoints)
                    CGame.Instance.AddScore(1);

                //destroy this entity
                CEntityManager.Instance.DestroyEntity(this);
            }

            return;
        }
        //override the CEntity.CollisionEvent() function
        public override void CollisionEvent(IPhysicalable obj)
        {
            //check that it is a project it is colliding with
            if (CEntityManager.Instance.GetEntity(obj).IsProjectile)
            {
                //if it is then we dont want it to add points 
                m_bScorePoints = false;
                //and we need to set the is alive flag to false so it is removed in the next call to CBox.Update
                m_bIsAlive = false;
            }
        }
    }
}