﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Interfaces
{
    //interface to expose required variables for renderer without the renderer knowing the entity itself
    interface  IRenderable
    {
        //Current position
        Math.Vector2D Pos
        {
            //the parent class is required to define get
            get;
        }
        //previous position
        Math.Vector2D PrevPos
        {
            //the parent class is required to define get
            get;
        }
        //boolean wether or not to draw geom
        bool Draw
        {
            //the parent class is required to define set and get
            set; get;
        }
        //store the geometry
        CGeometry Geometry
        {
            //the parent class is required to define get
            get;
        }
    }
}
