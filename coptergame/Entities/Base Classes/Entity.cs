﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Entities 
{
    //this class will be the base for all entities
    class CEntity : Interfaces.IRenderable, Interfaces.IPhysicalable
    {
        public CEntity()
        {
            return;
        }
        //using protected for variables as I want parent classes to be able to access them directly
        protected bool m_bIsProjectile = false;
        public bool IsProjectile
        {
            get
            {
                return m_bIsProjectile;
            }
        }
        protected bool m_bScorePoints = true;
        public bool ScorePoints
        {
            get
            {
                return m_bScorePoints;
            }
        }

        protected bool m_bIsPowerUp = false;
        public bool IsPowerUp
        {
            get
            {
                return m_bIsPowerUp;
            }
        }
        protected bool m_bIsPlayer = false;
        public bool IsPlayer
        {
            get
            {
                return m_bIsPlayer;
            }
        }
        protected bool m_bIsAlive = true;
        public bool IsAlive
        {
            get
            {
                return m_bIsAlive;
            }
        }

        //store current pos of entity
        protected Math.Vector2D m_2dPos = new Math.Vector2D(0, 0);
        //property to get and set the current pos
        public Math.Vector2D Pos
        {
            set
            {
                m_2dPos = value;
            }
            //when the position is retrived perform some checks to see if it is out of the game area bounds
            get
            {
                //if the x pos is less then DrawXStart
                if (m_2dPos.X < CCanvas.Instance.DrawXStart)
                {
                    //reset the position so it wont go out of bounds
                    m_2dPos = new Math.Vector2D(CCanvas.Instance.DrawXStart, m_2dPos.Y);
                    //set the alive flag to false
                    m_bIsAlive = false;
                }

                if (m_2dPos.Y < CCanvas.Instance.DrawYStart)
                {
                    //reset the position so it wont go out of bounds
                    m_2dPos = new Math.Vector2D(m_2dPos.X, CCanvas.Instance.DrawYStart);
                    //set the alive flag to false
                    m_bIsAlive = false;
                }

                //subtract 1 as the top left is the first characters position
                if ((m_2dPos.X + m_Geometry.Dimensions.X - 1) > (CCanvas.Instance.DrawXEnd))
                {
                    //reset the position so it wont go out of bounds
                    m_2dPos = new Math.Vector2D(CCanvas.Instance.DrawXEnd - m_Geometry.Dimensions.X - 1, m_2dPos.Y);
                    //set the alive flag to false
                    m_bIsAlive = false;
                }

                //subtract 2 as the top left is the first characters position and for some reason there is an offset of one I think it is to do with how the border is drawn
                if ((m_2dPos.Y + m_Geometry.Dimensions.Y - 2) > CCanvas.Instance.DrawYEnd)
                {
                    //reset the position so it wont go out of bounds
                    m_2dPos = new Math.Vector2D(m_2dPos.X, CCanvas.Instance.DrawYEnd - m_Geometry.Dimensions.Y);
                    //set the alive flag to false
                    m_bIsAlive = false;
                }

                //return the checked position
                return m_2dPos;
            }
        }
        //store previous pos of entity
        protected Math.Vector2D m_2dPrevPos = new Math.Vector2D(0, 0);
        //propert to get the previous pos
        public Math.Vector2D PrevPos
        {
            get
            {
                return m_2dPrevPos;
            }
        }
        //store the dimensions
        protected Math.Vector2D m_2dDimensions = new Math.Vector2D(0, 0);
        //property to get the dimensions of geom
        public Math.Vector2D Dimensions
        {
            get
            {
                return m_2dDimensions;
            }
        }
        //store wether or not the entity should be drawn
        protected bool m_bDraw = false;
        //property to retrive wether or not to draw the entity
        public bool Draw
        {
            set
            {
                m_bDraw = value;
            }
            get
            {
                return m_bDraw;
            }
        }
        //store the geometry for the entity
        protected CGeometry m_Geometry = null;
        //property to get the geometry for the entity
        public CGeometry Geometry
        {
            get
            {
                return m_Geometry;
            }
        }
        //virtual functions that allow the parent class to extend any basic functionality
        //this is called for both entities that took part in a collision
        public virtual void CollisionEvent(Interfaces.IPhysicalable obj)
        {
            return;
        }
        //this is called per frame
        public virtual void Update()
        {
            return;
        }
        //this is called by the entity manager when an entity is destroyed
        public virtual void Destroy()
        {
            return;
        }
    }
}