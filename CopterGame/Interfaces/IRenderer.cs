﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Interfaces
{
    class IRenderer
    {


        Math.Vector2D Pos
        {
            get;
        }

        Math.Vector2D PrevPos
        {
            get;
        }

        Math.Vector2D Dimensions
        {
            get;
        }
    }
}
