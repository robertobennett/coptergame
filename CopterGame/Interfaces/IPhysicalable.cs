﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Interfaces
{
    //interface to expose necessary properties to the physics class
    interface IPhysicalable
    {
        //Current position
        Math.Vector2D Pos
        {
            get;
        }
        //previous position
        Math.Vector2D PrevPos
        {
            get;
        }
        //dimensions of the object
        Math.Vector2D Dimensions
        {
            get;
        }
    }
}