﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace CopterGame
{
    //this class is designed to manage the console window and the drawing window and try to make sure the console is always maximized - this class follows the singleton pattern
    //UPDATE -- I wanted to fix the console flickering, I realised that if I was drawing 40 characters to the console 30 seconds using Console.SetPos and Console.Write I would be making
    //2400 calls to the console, the console is not designed to be able to keep up with that many calls, to fix it I modified this class to have a set position and a write method
    //that will write to a 2d array that acts as a buffer, this array is then converted to a string and is drawn to the console with one call of the console.write method

    //This class is designed to handle the drawing of geometry to the screen by storing the data that would of been sent to the console via Console.SetCursorPos(x, y) and Console.Write(c)
    //in a 2d array(List) that I will refer to as the buffer, I chose to do it this way because of the fickering, it came to me that I was calling Console.SetCursorPos(...) and Console.Write(...) one for each character in my geometries
    //some quick maths says that 10 objects of 10 characters drawn 30 times per second results in 10 * 10 * 30 * 2 = 6000 calls to the Console the console simply isnt desinged to deal with this, to get around this I designed the class
    //to convert the data in the buffer to a string and then simply call Console.WriteLine(...) once per frame (~30 times per second) this massively reduces the overhead 
    class CCanvas
    {
        //import the get console window function from the kernel32 library this will return a handle to the console window for use in other imported windows functions
        [DllImport("kernel32")]
        public static extern IntPtr GetConsoleWindow();

        //the function imported below will be used to maximize the console window
        const uint m_uiShowWindow_Maximize = 3;
        [DllImport("user32")]
        public static extern bool ShowWindow(IntPtr hwnd, uint swCmd);

        //import the delete menu function from the user32.dll this will disable certain aspects of a menu - the constants below are relevant to delete particular menus from the console window
        private const int m_uiDeleteMenuByCommand = 0x00000000;
        private const int m_uiDeleteMenuByPosition = 0x00000400;
        private const int m_uiDeleteMenu_Close = 0xF060;
        private const int m_uiDeleteMenu_Minimze = 0xF020;
        private const int m_uiDeleteMenu_Maximize = 0xF030;
        private const int m_uiDeleteMenu_Restore = 0xF120;
        private const int m_uiDeleteMenu_Move = 0xF010;
        private const int m_uiDeleteMenu_HScroll = 0xF080;
        private const int m_uiDeleteMenu_Size = 0xF000;
        [DllImport("user32")]
        public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        //get system menu - this will return a pointer to the system menu for the window passed to it (system menu includes title, close, minimize, maximize and certain commands
        [DllImport("user32")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        //store an instance of the only instance of this class
        static private CCanvas m_Instance = null;

        //store the number of columns in the console buffer
        private int m_iConsoleColumns = 0;
        //store the number of rows in the console buffer
        private int m_iConsoleRows = 0;
        //store the x position that the geometry can be drawn from - this allows a margin to be added around the play area
        private int m_iDrawXStart = 0;
        //property to provide "read" access
        public int DrawXStart
        {
            get
            {
                return m_iDrawXStart;
            }
        }
        //store the last possible x value that the geom can be drawn to - this again allows a margin to be added around the play area
        private int m_iDrawXEnd = 0;
        public int DrawXEnd
        {
            get
            {
                return m_iDrawXEnd - 1;
            }
        }
        //same theory as x start
        private int m_iDrawYStart = 0;
        //property to provide "read" access
        public int DrawYStart
        {
            get
            {
                return m_iDrawYStart;
            }
        }
        //same theory as x end
        private int m_iDrawYEnd = 0;
        //property to provide "read" access
        public int DrawYEnd
        {
            get
            {
                return m_iDrawYEnd - 1;
            }
        }
        //width of the draw area - note this is not the same as the width of the console window
        private int m_iWidth = 0;
        //property to provide "read" access
        public int Width
        {
            get
            {
                return m_iWidth;
            }
        }
        //height of the draw area - note this is not the same as height of the console window
        private int m_iHeight = 0;
        //property to provide "read" access
        public int Height
        {
            get
            {
                return m_iHeight;
            }
        }

        //This is drawn at the top of the scree and is designed for subtle hints alought I refactored it to hold the score its feature is there if I wanted to use it later
        private String m_sHint = "Be prepared...";
        //property to provide "read" and "write" access
        public String Hint
        {
            get
            {
                return m_sHint;
            }
            set
            {
                m_sHint = value;
                this.DrawHint();
            }
        }

        //2d array(list) acts in the same way as the console buffer does
        List<List<char>> m_Canvas = new List<List<char>>();
        //hide the constructor so it can only be called from within this class
        private CCanvas()
        {
            //maximize the console window
            ShowWindow(GetConsoleWindow(), m_uiShowWindow_Maximize);
            //delete menu items
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), m_uiDeleteMenu_Close, m_uiDeleteMenuByCommand);
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), m_uiDeleteMenu_Minimze, m_uiDeleteMenuByCommand);
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), m_uiDeleteMenu_Maximize, m_uiDeleteMenuByCommand);
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), m_uiDeleteMenu_Restore, m_uiDeleteMenuByCommand);
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), m_uiDeleteMenu_Move, m_uiDeleteMenuByCommand);
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), m_uiDeleteMenu_Move, m_uiDeleteMenuByPosition);
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), m_uiDeleteMenu_HScroll, m_uiDeleteMenuByCommand);
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), m_uiDeleteMenu_Size, m_uiDeleteMenuByCommand);

            //hide the console cursor so it doesnt jump all over the place when Console.SetCursorPos(...) is called
            Console.CursorVisible = false;

            //set the console buffer dimensions to that of the window its self
            Console.BufferWidth = Console.WindowWidth;
            Console.BufferHeight = Console.WindowHeight;
            //store the console buffer dimensions, 1 width unit is one column and 1 row unit is one row
            m_iConsoleColumns = Console.BufferWidth;
            //needs to be one below buffer hight so it doesnt scroll the console
            m_iConsoleRows = Console.BufferHeight - 1;

            //initialize draw x,y start and end
            m_iDrawXStart = 5;
            //for reasons unknown to me, to get it to look symmetrical logic says to subtract 5, im guessing that the way it is set up is actually 1 char from the end then 1 char that contains the \n
            m_iDrawXEnd = m_iConsoleColumns - 3;
            m_iDrawYStart = 3;
            m_iDrawYEnd = m_iConsoleRows - 3;
            m_iWidth = m_iDrawXEnd - m_iDrawXStart;
            m_iHeight = m_iDrawYEnd - m_iDrawYStart;

            for (int y = 0; y < m_iConsoleRows; y++)
            {
                //Create a list to store the row
                List<char> row = new List<char>();
                //row need same number of columns console, as we are initializing canvase fill all but last character (as it needs to be a end of line character)
                for (int x = 0; x < m_iConsoleColumns - 1; x++)
                {
                    row.Add(' ');
                }
                //add last char which needs to be EOL char
                row.Add('\n');
                //now add the row to the canvas
                m_Canvas.Add(row);
            }

            return;
        }
        //create a static access to the instance, so that it can return one to access the static functions
        static public CCanvas Instance
        {
            get
            {
                //check if the instance exsists
                if(m_Instance == null)
                {
                    //if it doesnt create an instance
                    m_Instance = new CCanvas();
                    return m_Instance;
                }

                //other wise just return the already created instance
                return m_Instance;
            }
        }

        //stores the position to insert the next char that is written to the buffer
        private int[] m_iPos = new int[2];
        //function to set the cursor pos, didnt want to use a property, felt a function had a feel more like the console
        public void SetCursorPosition(int x, int y)
        {
            //some error handling to check if the cursor pos is set beyond the dimensions of the canvas... this would throw an out of bounds exception anyways if t was attempeted to write to that pos but
            //this catches it early and is more detailed
            if (x >= m_iConsoleColumns)
                throw new Exception("X coord (" + x.ToString() + ") is creater then the buffer width (" + (m_iConsoleColumns - 1).ToString() + ")");

            if (y >= m_iConsoleRows)
                throw new Exception("Y coord (" + y.ToString() + ") is creater then the buffer width (" + (m_iConsoleRows - 1).ToString() + ")");

            //set the pos...
            m_iPos[0] = x;
            m_iPos[1] = y;
        }
        //clears the buffer and draws the frame if the flag is enabled and draws the hint if the hint is enabled
        public void Clear(bool drawBorder = true, bool drawHint = true)
        {
            //clear the canvas (reset everything with blank chars)
            for (int y = 0; y < m_Canvas.Count; y++)
            {
                //reset all but the last character
                for(int x = 0; x < m_Canvas[y].Count - 1; x++)
                {
                    m_Canvas[y][x] = ' ';
                }
            }

            //if draw border flag is true then call draw border
            if (drawBorder)
                DrawBorder();

            //if draw hint flag is true then call draw hint
            if(drawHint)
               DrawHint();
        }

        public void DrawBorder()
        {
            //between the x start and end pos draw '-' char
            for (int x = m_iDrawXStart; x < m_iDrawXEnd; x++)
            {
                //draw it on the row before and row after y start and end respectivly
                m_Canvas[m_iDrawYStart - 1][x] = '-';
                m_Canvas[m_iDrawYEnd + 1][x] = '-';
            }

            //between the y start and end pos draw 'Z' char
            for (int y = m_iDrawYStart; y < m_iDrawYEnd + 1; y++)
            {
                //draw it on the column before and row after x start and end respectivly
                m_Canvas[y][m_iDrawXStart - 1] = '|';
                m_Canvas[y][m_iDrawXEnd + 1] = '|';
            }

            return;
        }

        public void DrawHint()
        {
            //simply draw the hint
            for (int i = 0; i < m_sHint.Length; i++)
            {
                //on the second row, column of x start
                m_Canvas[1][m_iDrawXStart + i] = m_sHint[i];
            }
        }

        public void Write(char c)
        {
            //simply set the character at the position stored to the param c
            m_Canvas[m_iPos[1]][m_iPos[0]] = c;
        }
        //this function converts the canvas buffer from a 2d array (list) to a string that can be drawin with Console.Write(...)
        public void PresentCanvas()
        {
            //create a blank string
            String s = "";
            //for every row in buffer
            for (int y = 0; y < m_Canvas.Count; y++)
            {
                //for every column in buffer
                for (int x = 0; x < m_Canvas[y].Count; x++)
                {
                    //add that character
                    s += m_Canvas[y][x];
                }
            }
            //set cursor to top pos so it will overwrite exsisting and not start a new "page"
            Console.SetCursorPosition(0, 0);
            //write the string 
            Console.Write(s);
        }
        public void Shutdown()
        {

        }
    }
}
