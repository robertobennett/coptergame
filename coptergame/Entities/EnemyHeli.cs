﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using CopterGame.Interfaces;

namespace CopterGame.Entities
{
    //This is the enemy version of the player helicopter it will launch rockets every three seconds at the players current
    //position, it uses a timer so needs to inherit from IDisposable so the timer is definatly disposed off
    class CEnemyHeli : CEntity, IDisposable
    {
        //inherited from IDisposable
        public void Dispose()
        {
            //if the timer exsits
            if (m_RocketFireTimer != null)
            {
                //stop it
                m_RocketFireTimer.Stop();
                //dispose of it
                m_RocketFireTimer.Dispose();
                //set its instance to null so it is destroyed
                m_RocketFireTimer = null;
            }

            return;
        }

        //all instances of CEnemyHeli can share one instance of the geometry so this saves alot of memory and performance
        private static CGeometry sm_HeliGeom = new CGeometry(CGeometry.GetGeomDir() + "EnemyHelicopter.geom");
        //create a variable for the timer and initailize it to null
        private Timer m_RocketFireTimer = null;
        public CEnemyHeli()
        {
            //set the geometry variable (inhertied from CEntity) to that static instance of the geometry
            this.m_Geometry = sm_HeliGeom;
            //set that bDraw flag to true as we want it to be drawn
            this.m_bDraw = true;
            //set the initial 2dPos this will almost certainly not be the actual position on creation, initializing to this as the object wont intstantly be destroyed
            this.m_2dPos = new Math.Vector2D(CCanvas.Instance.DrawXStart, CCanvas.Instance.DrawYStart);
            //intialize the previous pos to the start pos
            this.m_2dPrevPos = this.m_2dPos;
            //set the dimensions to match that in the geometry
            this.m_2dDimensions = this.m_Geometry.Dimensions;

            //every 3 seconds fire a rocket
            m_RocketFireTimer = new Timer(3000);
            //start the timer
            m_RocketFireTimer.Start();
            //add the timer elapsed event
            m_RocketFireTimer.Elapsed += (Object source, ElapsedEventArgs e) => 
            {
                //create a rocket entity
                CRocket rocket = CEntityManager.Instance.CreateEntity<CRocket>();
                //set the rocket pos to just infront of the helicopter
                rocket.Pos = this.Pos.Add(new Math.Vector2D(-CRocket.GeomWidth - 1, 1));
                //set the target pos to be the current position of the player
                rocket.TargetPos = CGame.Instance.PlayerPos;
            };


            return;
        }
        //called by entity manager
        public override void Destroy()
        {
            //check if the timer instance is null
            if (m_RocketFireTimer != null)
            {
                //if it isnt stop the timer
                m_RocketFireTimer.Stop();
                //dispose of it
                m_RocketFireTimer.Dispose();
                //and set the var to null
                m_RocketFireTimer = null;
            }
        }
        //called per frame
        public override void Update()
        {
            //update the prev pos
            this.m_2dPrevPos = m_2dPos;
            //move the heli one unit left
            m_2dPos = m_2dPos.Add(new Math.Vector2D(-1, 0));
            //check if the heli is still alive
            if (!m_bIsAlive)
            {
                //if it isnt alive check if it can give points if it can add 15
                if(m_bScorePoints)
                    CGame.Instance.AddScore(15);
                //destroy this entity
                CEntityManager.Instance.DestroyEntity(this);
            }

            return;
        }
        //called if this collides
        public override void CollisionEvent(IPhysicalable obj)
        {
            //if it collides with a porjectile
            if (CEntityManager.Instance.GetEntity(obj).IsProjectile)
            {
                //disable and remove the timer
                if (m_RocketFireTimer != null)
                {
                    m_RocketFireTimer.Stop();
                    m_RocketFireTimer.Dispose();
                    m_RocketFireTimer = null;
                }
                //cannot score points
                m_bScorePoints = false;
                //set the is alive flag to false
                m_bIsAlive = false;
            }
            
            base.CollisionEvent(obj);
        }
    }
}