﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame
{
    //this class handles the physics, only one instance is needed so using singleton pattern
    class CPhysics
    {
        //store instance of class privatly
        private static CPhysics sm_Instance = null;
        //property to get the instance
        public static CPhysics Instance
        {
            get
            {
                //if null create the instance
                if (sm_Instance == null)
                {
                    sm_Instance = new CPhysics();
                }
                //return instnace
                return sm_Instance;
            }
        }
        //hide constructor as using singleton pattern
        private CPhysics()
        {
            return;
        }
        //store list of registered physicables
        List<Interfaces.IPhysicalable> m_Physicalbles = new List<Interfaces.IPhysicalable>();
        //function to register a physicable
        public bool RegisterPhysicalable(Interfaces.IPhysicalable obj)
        {
            if (obj == null)
                return false;

            //if the physicable count is greater then zero, check if it is already registered, if it is return false
            if(m_Physicalbles.Count > 0)
            {
                for(int i = 0; i < m_Physicalbles.Count; i++)
                {
                    if (m_Physicalbles[i] == obj)
                        return false;
                }
            }

            //it wasnt already registered so register physicable and return true
            m_Physicalbles.Add(obj);

            return true;
        }
        //fucntion to unregister physicable
        public bool UnregisterPhysicalable(Interfaces.IPhysicalable obj)
        {
            if (obj == null)
                return false;
            //if there are physcables registered
            if (m_Physicalbles.Count > 0)
            {
                //for all physicables
                for (int i = 0; i < m_Physicalbles.Count; i++)
                {
                    //check if iterated is equal to obj
                    if (m_Physicalbles[i] == obj)
                    {
                        //if it is remove it and return, can only be one instance of it registered due to the way registerrenderable is written
                        m_Physicalbles.RemoveAt(i);
                        return true;
                    }
                }
            }

            return false;
        }
        //called per frame
        public void Update()
        {
            //if more then 1 physicable registered
            if (m_Physicalbles.Count > 1)
            {
                //for all of them
                for (int x = 0; x < m_Physicalbles.Count; x++)
                {
                    //for all of the rest of them - set up like this so the check is never repeated
                    for (int y = x + 1; y < m_Physicalbles.Count; y++)
                    {
                        //perform collision check
                        if(CollisionCheck(m_Physicalbles[x], m_Physicalbles[y]))
                        {
                            //if collision check returns true then call collision event for each physicable and pass the other physicable across
                            CEntityManager.Instance.GetEntity(m_Physicalbles[x]).CollisionEvent(m_Physicalbles[y]);
                            CEntityManager.Instance.GetEntity(m_Physicalbles[y]).CollisionEvent(m_Physicalbles[x]);
                        }
                    }
                }
            }

            return;
        }
        //this collision check uses seperating axis theorem to check if the x and y component for the vector between the two object centers is less then the sum of the half widths of the objects
        private bool CollisionCheck(Interfaces.IPhysicalable obj1, Interfaces.IPhysicalable obj2)
        {
            //calculate the object centers
            Math.Vector2D obj1Center = new Math.Vector2D(obj1.Dimensions.X / 2, obj1.Dimensions.Y / 2);
            Math.Vector2D obj2Center = new Math.Vector2D(obj2.Dimensions.X / 2, obj2.Dimensions.Y / 2);
            //translate the centers by the postions of the objects
            obj1Center = obj1Center.Add(obj1.Pos);
            obj2Center = obj2Center.Add(obj2.Pos);
            //calculate the vector of object 2 to one or the otherway around
            Math.Vector2D obj2ToObj1 = obj1Center.Subtract(obj2Center);
            //calculate the sum of the dimensions of two objects
            Math.Vector2D dim = obj1.Dimensions.Add(obj2.Dimensions);

            //do the check as described, added offset this is to do with the fact that the console only supports intergers and my geometry is odd number of rows/characters - if teh conditions are true return true
            if (obj2ToObj1.Xabs * 2 <= dim.Xabs - 1 && obj2ToObj1.Yabs * 2 <= dim.Yabs - 1)
                return true;

            //else return false
            return false;
        }
    }
}
