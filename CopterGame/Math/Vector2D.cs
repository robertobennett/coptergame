﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Math
{
    //vector class to represent a 2d vector (use dynamic so the type can be specified ie int, float, double...
    class Vector2D
    {
        //store the x and y pos
        private dynamic m_x, m_y;
        //provide a property to retrive the X value
        public dynamic X
        {
            get
            {
                return m_x;
            }
        }
        //provide a property to retrive the Y value
        public dynamic Y
        {
            get
            {
                return m_y;
            }
        }

        //provide a property to retrive the X value
        public dynamic Xabs
        {
            get
            {
                if(m_x < 0)
                    return -m_x;

                return m_x;
            }
        }
        //provide a property to retrive the Y value
        public dynamic Yabs
        {
            get
            {
                if (m_y < 0)
                    return -m_y;

                return m_y;
            }
        }
        public Vector2D Scale(dynamic sf)
        {
            return new Vector2D(m_x * sf, m_y * sf);
        }
        //constructor takes the inital x and y position
        public Vector2D(dynamic x, dynamic y)
        {
            m_x = x;
            m_y = y;
        }
        //add method returns a new vector with values of this vector + the one passed in
        public Vector2D Add(Vector2D vec2D)
        {
            return new Vector2D(m_x + vec2D.X, m_y + vec2D.Y);
        }
        //subtract method returns a new cector with the value of this vector - the one passed in
        public Vector2D Subtract(Vector2D vec2D)
        {
            return new Vector2D(m_x - vec2D.X, m_y - vec2D.Y); ;
        }
        //calculate and returns the magnitude of the vector
        public dynamic Magnitude()
        {
            return System.Math.Sqrt((m_x * m_x) + (m_y * m_y));
        }
        //calculate and returns the dot product of the this vector and the one passed in
        public double DotProduct(Vector2D vec2D)
        {
            return ((this.m_x * vec2D.X) + (this.m_y * vec2D.Y));
        }
        //calculate and returns the angle between the two vectors
        public double AngleBetween(Vector2D vec2D)
        {
            return System.Math.Acos((DotProduct(vec2D) / (this.Magnitude() * vec2D.Magnitude())));
        }
    }
}