﻿using CopterGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Entities
{
    //this is a basic powerup that simply doubles your points when the player collides with it
    class CPowerUpX2 : CEntity
    {
        //all instances of CPowerUpX2 can share one instance of the geometry so this saves alot of memory and performance
        private static CGeometry sm_x2 = new CGeometry(CGeometry.GetGeomDir() + "PowerUpX2.geom");
        //public constructor as it will be called by the entity manager
        public CPowerUpX2()
        {
            this.m_Geometry = sm_x2;
            this.m_bDraw = true;
            this.m_2dPos = new Math.Vector2D(CCanvas.Instance.DrawXStart + 90, CCanvas.Instance.DrawYStart + 20);
            this.m_2dPrevPos = this.m_2dPos;
            this.m_2dDimensions = this.m_Geometry.Dimensions;
            this.m_bIsPowerUp = true;

            return;
        }
        //called per frame
        public override void Update()
        {
            //update previous position
            this.m_2dPrevPos = m_2dPos;
            //move power up one to the left
            this.m_2dPos = this.m_2dPos.Add(new Math.Vector2D(-1, 0));

            //if the power up isnt alive remove it
            if (!m_bIsAlive)
            {
                CEntityManager.Instance.DestroyEntity(this);
            }

            return;
        }

        public override void CollisionEvent(IPhysicalable obj)
        {
            //check for a collision with the player
            if (CEntityManager.Instance.GetEntity(obj).IsPlayer)
            {
                //if it collides with the player, double the score
                CGame.Instance.AddScore(CGame.Instance.Score);
                //destroy the entity imediatly
                CEntityManager.Instance.DestroyEntity(this);
            }
            //if the collision wasnt with the player mark it for removal
            m_bIsAlive = false;

            base.CollisionEvent(obj);
        }
    }
}