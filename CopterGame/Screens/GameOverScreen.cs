﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Screens
{
    //this class is going to define the game over screen - this class uses legacy code and could be updated with the use of the font manager
    class CGameOverScreen : Interfaces.IKeyListener
    {
        //create an action function that fits the delegate type CMenuItem.voidfuncptr to be tiggered for the return to menu, menu item
        private void ReturnToMenuAction()
        {
            //when triggered post the return to menu message
            CGame.Instance.PostMessage(CGame.GameMessages.RETURN_TO_MENU);
        }
        //create an action function that fits the delegate type CMenuItem.voidfuncptr to be tiggered for the exit to desktop, menu item
        private void ExitToDesktop()
        {
            //when triggered post the exit message
            CGame.Instance.PostMessage(CGame.GameMessages.EXIT);
        }

        //create the nessicary geometry for the text
        private CGeometry m_GameOverText = new CGeometry(CGeometry.GetGeomDir() + "GameOver.font");
        private CGeometry m_ScoreText = new CGeometry(CGeometry.GetGeomDir() + "Score.font");
        private CGeometry m_One = new CGeometry(CGeometry.GetGeomDir() + "One.font");
        private CGeometry m_Two = new CGeometry(CGeometry.GetGeomDir() + "Two.font");
        private CGeometry m_Three = new CGeometry(CGeometry.GetGeomDir() + "Three.font");
        private CGeometry m_Four = new CGeometry(CGeometry.GetGeomDir() + "Four.font");
        private CGeometry m_Five = new CGeometry(CGeometry.GetGeomDir() + "Five.font");
        private CGeometry m_Six = new CGeometry(CGeometry.GetGeomDir() + "Six.font");
        private CGeometry m_Seven = new CGeometry(CGeometry.GetGeomDir() + "Seven.font");
        private CGeometry m_Eight = new CGeometry(CGeometry.GetGeomDir() + "Eight.font");
        private CGeometry m_Nine = new CGeometry(CGeometry.GetGeomDir() + "Nine.font");
        private CGeometry m_Zero = new CGeometry(CGeometry.GetGeomDir() + "Zero.font");

        //a function to convert a character to its ascii art equivialnt
        private CGeometry CharToGeom(char c)
        {
            switch(c)
            {
                default:
                    throw new Exception("Cannot convert char to geom");

                case '0':
                    return m_Zero;

                case '1':
                    return m_One;

                case '2':
                    return m_Two;

                case '3':
                    return m_Three;

                case '4':
                    return m_Four;

                case '5':
                    return m_Five;

                case '6':
                    return m_Six;

                case '7':
                    return m_Seven;

                case '8':
                    return m_Eight;

                case '9':
                    return m_Nine;
            }
        }

        //called by the key manager
        public void KeyEvent(ConsoleKeyInfo cki)
        {
            //only handle a key press if the menu is visable
            if (m_bVisable)
            {
                //switch statment for the key pressed
                switch (cki.Key)
                {
                    default:
                        break;

                    //when up arrow is pressed move between menu items
                    case ConsoleKey.LeftArrow:
                        {
                            //if the new position in the item array will be less then zero, set it to the end of the list
                            if (m_iMenuItemSelected - 1 < 0)
                            {
                                m_MenuItems[m_iMenuItemSelected].Selected = false;
                                m_iMenuItemSelected = m_MenuItems.Count - 1;
                                m_MenuItems[m_iMenuItemSelected].Selected = true;
                                //redraw the menu
                                Redraw();
                                break;
                            }

                            //otherwise just decrement the selected item and update the items
                            m_MenuItems[m_iMenuItemSelected].Selected = false;
                            m_iMenuItemSelected--;
                            m_MenuItems[m_iMenuItemSelected].Selected = true;
                            //redraw the menu
                            Redraw();

                            break;
                        }

                    //when down arrow is pressed move between menu items
                    case ConsoleKey.RightArrow:
                        {
                            //check if the next one is going to go past the end of the array then set it back to beginning
                            if (m_iMenuItemSelected + 1 > m_MenuItems.Count - 1)
                            {
                                //set the current iterate item selected to fales
                                m_MenuItems[m_iMenuItemSelected].Selected = false;
                                //reset item selected to begining
                                m_iMenuItemSelected = 0;
                                //set the new item selected, selected to true
                                m_MenuItems[m_iMenuItemSelected].Selected = true;
                                //redraw the menu
                                Redraw();
                                break;
                            }

                            //otherwise just move it down
                            //set the currently selected item, selected to false
                            m_MenuItems[m_iMenuItemSelected].Selected = false;
                            //increment the item selected
                            m_iMenuItemSelected++;
                            //set the newly iterated item selected, selected to true
                            m_MenuItems[m_iMenuItemSelected].Selected = true;
                            //redraw the menu
                            Redraw();
                            break;
                        }

                    //when enter key is pressed call the action for the menu item
                    case ConsoleKey.Enter:
                        {
                            //check if the iterated item has an action defined then calle it
                            if (m_MenuItems[m_iMenuItemSelected].Action != null)
                                m_MenuItems[m_iMenuItemSelected].Action();

                            break;
                        }
                }

                return;
            }

            return;
        }

        //using a singleton so we need to store the only instance of the class
        static private CGameOverScreen sm_Instance = null;
        //create a property to retrive the instance
        static public CGameOverScreen Instance
        {
            get
            {
                //check if the instance has been created
                if (sm_Instance == null)
                {
                    //if not create it and return it
                    sm_Instance = new CGameOverScreen();
                    return sm_Instance;
                }

                //has already been created so return it
                return sm_Instance;
            }
        }

        //store the index of the currently selected menu item
        private int m_iMenuItemSelected = 0;
        //create a list of the menu items
        private List<CMenuItem> m_MenuItems = new List<CMenuItem>();

        //make the constructor private as its a singleton
        private CGameOverScreen()
        {
            CKeyManager.Instance.RegisterListener(this);

            return;
        }

        //function to initialize the menu
        public void Initialize()
        {
            int height = (int)((CCanvas.Instance.Height / 5) + 0.5f);
            //create a menu item for each option and add them to the list /--/
            CMenuItem returnToMenu = new CMenuItem(CCanvas.Instance.DrawXStart + 10, CCanvas.Instance.DrawYEnd - height - 5, "Return To Menu", true);
            returnToMenu.Selected = true;
            returnToMenu.Action = ReturnToMenuAction;

            m_MenuItems.Add(returnToMenu);

            //create a menu item for each option and add them to the list /--/
            CMenuItem exit = new CMenuItem(CCanvas.Instance.DrawXEnd - (int)((CCanvas.Instance.Width / 3) + 0.5f) - 10, CCanvas.Instance.DrawYEnd - height - 5, "Exit To Desktop", true);
            exit.Selected = false;
            exit.Action = ExitToDesktop;

            m_MenuItems.Add(exit);

            return;
        }

        //store whether or not the menu is visable
        private bool m_bVisable = false;
        public bool Visable
        {
            get
            {
                return m_bVisable;
            }
            set
            {
                //if the value set too is true
                if (value == true)
                {
                    //set visable var to true
                    m_bVisable = true;
                    //redraw the console
                    Redraw();
                }
                else
                {
                    //set visable var to false
                    m_bVisable = false;
                    //set state of currently selected menu item to not selected
                    m_MenuItems[m_iMenuItemSelected].Selected = false;
                    //reset the selected back to zero
                    m_iMenuItemSelected = 0;
                    //set state of newly selected menu item to selected
                    m_MenuItems[m_iMenuItemSelected].Selected = true;
                    //clear the menu of menu items to false
                    Clear();
                }
            }
        }

        public void Redraw()
        {
            //if the menu is visable the readraw all the items
            if (m_bVisable)
            {
                //draw the game over text
                //calculate x pos to center text on
                int xStart = (CCanvas.Instance.Width / 2) - (m_GameOverText.Dimensions.X / 2) + CCanvas.Instance.DrawXStart;
                //going to reuse the geom code to load some ascii art for game over
                //for all the rows in the geom
                for (int y = 0; y < m_GameOverText.Geometry.Count; y++)
                {
                    //for all the characters in the row
                    for(int x = 0; x < m_GameOverText.Geometry[y].Count; x++)
                    {
                        //set the cursor pos to the x start plus the iterated x pos and the same for the y
                        CCanvas.Instance.SetCursorPosition(xStart + x, CCanvas.Instance.DrawYStart + y);
                        //write thte iterated charcter
                        CCanvas.Instance.Write(m_GameOverText.Geometry[y][x]);
                    }
                }

                //convert score to geom array so width can be calculated
                //create empty string
                String s = "";
                //add the score to the string
                s += CGame.Instance.Score.ToString();
                //create an empty list to store the converted scores ascii art
                List<CGeometry> geom = new List<CGeometry>();
                //create a var to store the total width of the score as ascii art
                int iWidth = 0;
                //for the length of the string
                for (int i = 0; i < s.Length; i++)
                {
                    //add the iterated character of the string to the geom array after it has been converted to an ascii character
                    geom.Add(CharToGeom(s[i]));
                    //add to the width the width of that ascii character
                    iWidth += geom[i].Dimensions.X;
                }
                
                //draw the score: text
                //recalucate x pos so that the text is centered
                xStart = (CCanvas.Instance.Width / 2) - (iWidth / 2) - (m_ScoreText.Dimensions.X / 2) + CCanvas.Instance.DrawXStart;
                //going to reuse the geom code to load some ascii art for game over
                //for every row in the score text
                for (int y = 0; y < m_ScoreText.Geometry.Count; y++)
                {
                    //for every character in the row
                    for (int x = 0; x < m_ScoreText.Geometry[y].Count; x++)
                    {
                        //set the cursor pos to the x start plus the iterated x pos and the same for the y taking into account the height of the game over text and adding an offset
                        CCanvas.Instance.SetCursorPosition(xStart + x, CCanvas.Instance.DrawYStart + m_GameOverText.Geometry.Count + 2 + y);
                        //write the iterated character
                        CCanvas.Instance.Write(m_ScoreText.Geometry[y][x]);
                    }
                }

                //add to the x start the size of the Score: text and an offset
                xStart += m_ScoreText.Dimensions.X + 2;
                //iX  holds the x offset for drawing the numbers for the score
                int iX = 0;
                //for all the numbers in the score
                for (int i = 0; i < geom.Count; i++)
                {
                    //for all the rows in the iterated number
                    for (int y = 0; y < geom[i].Geometry.Count; y++)
                    {
                        //for all the characters in the rows
                        for (int x = 0; x < geom[i].Geometry[y].Count; x++)
                        {
                            //set the cursor pos to the x start plus the iterated x pos and the same for the y taking into account the height of the game over text and adding an offset
                            CCanvas.Instance.SetCursorPosition(xStart + iX + x, CCanvas.Instance.DrawYStart + m_GameOverText.Geometry.Count + 2 + y);
                            //write the iterated character
                            CCanvas.Instance.Write(geom[i].Geometry[y][x]);
                            
                        }
                    }
                    //add to the offset the width of the last number added and an offset
                    iX += geom[i].Dimensions.X + 1;
                }
                //now present the canvas
                CCanvas.Instance.PresentCanvas();
                //redraw the menu items as they are seperate to the actual text and score
                for (int i = 0; i < m_MenuItems.Count; i++)
                    m_MenuItems[i].Redraw();
            }
        }

        public void Clear()
        {
            //just call the clear method for each item
            for (int i = 0; i < m_MenuItems.Count; i++)
                m_MenuItems[i].Clear();
        }
    }
}