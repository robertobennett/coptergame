﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame
{
    //will manage drawing things (not menu) to the game
    //using a singleton - one console only needs one renderer
    class CRenderer
    {
        //with singleton stores the instance
        private static CRenderer sm_Instance = null;
        //property to retirve (and create the instance)
        public static CRenderer Instance
        {
            get
            {
                //if the instance is not created
                if(sm_Instance == null)
                {
                    //create a new instance
                    sm_Instance = new CRenderer();
                }
                //return the instance
                return sm_Instance;
            }
        }
        //hide the constructor
        private CRenderer()
        {
            return;
        }
        //create a list to store the renderables
        private List<Interfaces.IRenderable> m_Renderables = new List<Interfaces.IRenderable>();
        //method to register the renderable
        public bool RegisterRenderable(Interfaces.IRenderable renderable)
        {
            //if there are renderables registered check to see if any are this one if they are return false so it isnt registered again
            if (m_Renderables.Count > 0)
            {
                for (int i = 0; i < m_Renderables.Count; i++)
                {
                    if (m_Renderables[i] == renderable)
                        return false;
                }
            }
            //otherwise register it
            m_Renderables.Add(renderable);

            return true;
        }
        //method to unregister the renderabke
        public bool UnregisterRenderable(Interfaces.IRenderable renderable)
        {
            //if there are renderables registered then check if it is registered if it is then remove it and return ture
            if (m_Renderables.Count > 0)
            {
                for(int i = 0; i < m_Renderables.Count; i++)
                {
                    if (m_Renderables[i] == renderable)
                    {
                        m_Renderables.RemoveAt(i);
                        return true;
                    }
                }
            }
            //otherwise return false
            return false;
        }
        //method that is called before draw, will clear the canvas
        public void PreDraw()
        {
            //just clear the canvas
            CCanvas.Instance.Clear();
            return;
        }
        //method to draw the renderables to the screen
        public void Draw()
        {
            if (m_Renderables.Count > 0)
            {
                //for every renderable
                for (int i = 0; i < m_Renderables.Count; i++)
                {
                    //if it is set to draw then draw it
                    if (m_Renderables[i].Draw)
                    {
                        //iterate through each row
                        for (int y = 0; y < m_Renderables[i].Geometry.Geometry.Count; y++)
                        {
                            //iterate through each column in the row
                            for (int x = 0; x < m_Renderables[i].Geometry.Geometry[y].Count; x++)
                            {
                                //set the cursor position to the previous position + the iterated x and y
                                CCanvas.Instance.SetCursorPosition(m_Renderables[i].Pos.X + x, m_Renderables[i].Pos.Y + y);
                                //write the character that is store in the geometry
                                CCanvas.Instance.Write(m_Renderables[i].Geometry.Geometry[y][x]);
                            }
                        }
                    }
                }
            }

            CCanvas.Instance.PresentCanvas();
            return;
        }
    }
}