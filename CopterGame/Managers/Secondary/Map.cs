﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Managers.Secondary
{
    //this class is responsable for generating the map, as only one instance is needed this is following the singleton pattern
    class CMap
    {
        //store the instance of the class privatly
        private static CMap sm_Instance = null;
        //store the random number generator
        private Random m_Generator = null;
        //store the current seed this can be used for debugging to create the map more then once if a bug occurs
        private int m_iCurrSeed = 0;
        //hide the constructor as it follows the singleton pattern
        private CMap()
        {
            m_iCurrSeed = Guid.NewGuid().GetHashCode();
            m_Generator = new Random(m_iCurrSeed);
            return;
        }
        //create a way to get the instance of the class
        public static CMap Instance
        {
            get
            {
                //if the instance is null then create it
                if (sm_Instance == null)
                    sm_Instance = new CMap();

                //return the instance
                return sm_Instance;
            }
        }

        //this method is used to reset the seed and the last created entity - prepares a new map to be generated
        public void Reset()
        {
            m_iCurrSeed = Guid.NewGuid().GetHashCode();
            m_Generator = new Random(m_iCurrSeed);
            m_LastCreated = null;
        }
        //store the last created so we can spawn another when it is a certain distance away...
        private Entities.CEntity m_LastCreated = null;
        //called every frame
        public void Update()
        {
            //if the last created entity is not null
            if (m_LastCreated != null)
            {
                //then check if it is far enough away from the end to create another
                if (m_LastCreated.Pos.X < CCanvas.Instance.DrawXEnd - (2 * m_LastCreated.Dimensions.X) - 5)
                {
                    //"do a roll" and if it is 22 then we can spawn the power up - idea is to make it random and depending on the limits passed it will vary how often
                    if (m_Generator.Next(1, 100) == 22)
                    {
                        //create an instance of the power up and store it as last instance
                        m_LastCreated = CEntityManager.Instance.CreateEntity<Entities.CPowerUpX2>();
                        //set the pos to a randomly generated position
                        m_LastCreated.Pos = new Math.Vector2D(CCanvas.Instance.DrawXEnd - m_LastCreated.Dimensions.X, m_Generator.Next(CCanvas.Instance.DrawYStart, CCanvas.Instance.DrawYEnd - m_LastCreated.Dimensions.Y));
                        return;
                    }
                    //same concept as the power up
                    else if(m_Generator.Next(1, 25) == 15)
                    {
                        //create an instance of the enemy heli and store it as last instance
                        m_LastCreated = CEntityManager.Instance.CreateEntity<Entities.CEnemyHeli>();
                        //set the pos to a randomly generated position
                        m_LastCreated.Pos = new Math.Vector2D(CCanvas.Instance.DrawXEnd - m_LastCreated.Dimensions.X, m_Generator.Next(CCanvas.Instance.DrawYStart, CCanvas.Instance.DrawYEnd - m_LastCreated.Dimensions.Y));
                        return;
                    }

                    //otherwise we can just spawn the standard obsticle
                    m_LastCreated = CEntityManager.Instance.CreateEntity<Entities.CBox>();
                    m_LastCreated.Pos = new Math.Vector2D(CCanvas.Instance.DrawXEnd - m_LastCreated.Dimensions.X, m_Generator.Next(CCanvas.Instance.DrawYStart, CCanvas.Instance.DrawYEnd - m_LastCreated.Dimensions.Y));
                }
            }
            else
            {
                //if it was spawn the first one
                m_LastCreated = CEntityManager.Instance.CreateEntity<Entities.CBox>();
                m_LastCreated.Pos = new Math.Vector2D(CCanvas.Instance.DrawXEnd - m_LastCreated.Dimensions.X, m_Generator.Next(CCanvas.Instance.DrawYStart + 3, CCanvas.Instance.DrawYEnd - m_LastCreated.Dimensions.Y - 3));
            }
        }
    }
}