﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Interfaces
{
    //interface for key presses, class inherits it and defines keyevent; then registers in keymanager and when a key is pressed the keymanager calls keyevent
    interface IKeyListener
    {
        //the parent class is required to define Key event, if the parent class registers with the key manager then it will have this function called on a key event
        void KeyEvent(ConsoleKeyInfo cki);
    }
}
