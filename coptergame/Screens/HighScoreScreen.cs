﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Screens
{
    //this screen will display the top three highscores
    class CHighScoreScreen : Interfaces.IKeyListener
    {
        //listen for key events so that the user can return to the main menu
        public void KeyEvent(ConsoleKeyInfo cki)
        {
            //if the screen is visable then act on key evetns
            if (m_bVisable)
            {
                switch (cki.Key)
                {
                    default:
                        break;
                    //when the escape key is pressed
                    case ConsoleKey.Escape:
                        //hide the highscore screen
                        Visable = false;
                        //display the main menu
                        CMainMenu.Instance.Visable = true;
                        break;
                }
            }

            return;
        }

        //hide constructor as using singleton
        private CHighScoreScreen()
        {
            CKeyManager.Instance.RegisterListener(this);
            return;
        }

        //using a singleton so we need to store the only instance of the class
        static private CHighScoreScreen sm_Instance = null;
        //create a property to retrive the instance
        static public CHighScoreScreen Instance
        {
            get
            {
                //check if the instance has been created
                if (sm_Instance == null)
                {
                    //if not create it and return it
                    sm_Instance = new CHighScoreScreen();
                    return sm_Instance;
                }

                //has already been created so return it
                return sm_Instance;
            }
        }

        //function to initialize the menu
        public void Initialize()
        {
            //hide the menu on initialize
            m_bVisable = false;
            return;
        }
        //variable to store if the menu is hidden
        private bool m_bVisable = false;
        //property to retrive and set if the menu is visable or not and get the if it is visable or not
        public bool Visable
        {
            get
            {
                return m_bVisable;
            }
            set
            {
                //if the value set too is true
                if (value == true)
                {
                    //set visable to true
                    m_bVisable = true;
                    //clear the screen
                    Console.Clear();
                    //draw the highscores
                    Redraw();
                }
                else
                {
                    //set visable var to false
                    m_bVisable = false;
                    //clear the canvas
                    CCanvas.Instance.Clear(false, false);
                    //clear the screen
                    Console.Clear();
                }
            }
        }

        public void Redraw()
        {
            //check if screen is visable
            if (m_bVisable)
            {
                //width of the geometry loaded
                int width = 0;
                //pass width as reference as this is set by that function but return type is a list of the actual geometry
                List<CGeometry> geoms = Managers.CFontManager.Instance.StringToGeom("high scores", ref width);
                //xS is x start which is set to the center point minus half of the width of the text
                int xS = CCanvas.Instance.DrawXStart + (CCanvas.Instance.Width / 2) - (width / 2);
                //yS is y start which is just an asthtically pleasing offset
                int yS = 2;
                //xO is x offset which is initailized to zero
                int xO = 0;

                //cear the canvas
                CCanvas.Instance.Clear(false, false);

                //wrtiting to canvas directly as we dont actually need to use color and this is alot quicker then individually drawing through Console.Write();
                //for all the geometries for the text
                for (int i = 0; i < geoms.Count; i++)
                {
                    //for all the rows in the iterated geometry
                    for (int y = 0; y < geoms[i].Geometry.Count; y++)
                    {
                        //for all the characters in the iterated row
                        for (int x = 0; x < geoms[i].Geometry[y].Count; x++)
                        {
                            //set the x position to the xstart plus the x offset plus the iterated x pos and the y pos to the y start plus the itererated y pos
                            CCanvas.Instance.SetCursorPosition(xS + xO + x, yS + y);
                            //write the iterated console character
                            CCanvas.Instance.Write(geoms[i].Geometry[y][x]);
                        }
                    }
                    //add the width of the iterated character to the x offset
                    xO += geoms[i].Dimensions.X;
                }

                //read the highscores to make sure they are up to date
                CGame.Instance.ReadHighScores();
                //update the y start to take into acount the height of the character drawn in the previous part and add an asthetically pleasing offset
                yS += 2 + geoms[0].Dimensions.Y;
                //for all the scores retrived
                for (int i = 0; i < CGame.Instance.Scores.Count; i++)
                {
                    //reset the width to zero as we will be retriving a new width
                    width = 0;
                    //width 1 is for the width of the player name whose highscore were reading
                    int width1 = 0;
                    //width 2 is for the width of the actual highscore for that player
                    int width2 = 0;
                    //create a list of geoms for the characters of the player name
                    List<CGeometry> nameGeoms = Managers.CFontManager.Instance.StringToGeom(CGame.Instance.Scores[i].sName, ref width1);
                    //create a list of geoms for te characters of the player score
                    List<CGeometry> scoreGeoms = Managers.CFontManager.Instance.StringToGeom(CGame.Instance.Scores[i].iScore.ToString(), ref width2);
                    //the width of the total geom is equal to the width of player name plus width of score plus offset (2)
                    width = width1 + width2 + 2;
                    //set x start so the text is centered
                    xS = CCanvas.Instance.DrawXStart + (CCanvas.Instance.Width / 2) - (width / 2);
                    //reset x offset
                    xO = 0;
                    //for all the characters in the name
                    for (int j = 0; j < nameGeoms.Count; j++)
                    {
                        //for all the rows in the character
                        for (int y = 0; y < nameGeoms[j].Geometry.Count; y++)
                        {
                            //foir all the characters in the row
                            for (int x = 0; x < nameGeoms[j].Geometry[y].Count; x++)
                            {
                                //set the x position to the xstart plus the x offset plus the iterated x pos and the y pos to the y start plus the itererated y pos
                                CCanvas.Instance.SetCursorPosition(xS + xO + x, yS + y);
                                //write the iterated console character
                                CCanvas.Instance.Write(nameGeoms[j].Geometry[y][x]);
                            }
                        }
                        //add the width of the iterated character to the x offset
                        xO += nameGeoms[j].Dimensions.X;
                    }

                    //add the spacing
                    xO += 2;

                    //for all the characters in the score
                    for (int j = 0; j < scoreGeoms.Count; j++)
                    {
                        //for all the rows in the character
                        for (int y = 0; y < scoreGeoms[j].Geometry.Count; y++)
                        {
                            //foir all the characters in the row
                            for (int x = 0; x < scoreGeoms[j].Geometry[y].Count; x++)
                            {
                                //set the x position to the xstart plus the x offset plus the iterated x pos and the y pos to the y start plus the itererated y pos
                                CCanvas.Instance.SetCursorPosition(xS + xO + x, yS + y);
                                //write the iterated console character
                                CCanvas.Instance.Write(scoreGeoms[j].Geometry[y][x]);
                            }
                        }
                        //add the width of the iterated character to the x offset
                        xO += scoreGeoms[j].Dimensions.X;
                    }

                    //add to the y start the height of the character drawn and an asthetically pleasing offset
                    yS += scoreGeoms[0].Dimensions.Y + 3;
                }

                //present the canvas now everything is drawn
                CCanvas.Instance.PresentCanvas();
            }
        }
    }
}