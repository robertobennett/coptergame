﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CopterGame
{
     //this class manages the key presses by listening for one and then calling a functon in the registered liseteners when a key is pressed
     //the class follows the singleton pattern as only one instance is needed
    class CKeyManager
    {
        //stores the instance of the singleton
        private static CKeyManager sm_Instance = null;
        //create a var to store the listening thread
        private Thread m_ListenThread = null;
        //create a property to retrive and/or create the instance
        public static CKeyManager Instance
        {
            get
            {
                //check if instance is created
                if (sm_Instance == null)
                {
                    //if it is create the instance
                    sm_Instance = new CKeyManager();
                    //return the newly created instance
                    return sm_Instance;
                }

                //instance was already created so just return it
                return sm_Instance;
            }
        }
        //hide the constructor so only the instance method can create the object
        private CKeyManager()
        {
            return;
        }
        public void Listen()
        {
            if (Console.KeyAvailable && m_KeyListeners.Count > 0)
            {
                //read the key
                ConsoleKeyInfo cki = Console.ReadKey(true);
                //for every registered listener
                for (int i = 0; i < m_KeyListeners.Count; i++)
                {
                    //call the key event function
                    m_KeyListeners[i].KeyEvent(cki);
                }
            }
        }
        //method to create the listening thread
        public void StartListening()
        {
            //check if the thread has been created, if it hasnt start listening
            if(m_ListenThread == null)
                m_ListenThread = new Thread(StartListenLoop);
            //start the thread
            m_ListenThread.Name = "Keystroke Listening Thread";
            m_ListenThread.Start();

            return;
        }
        //mthod to abort the listening thread
        public void StopListening()
        {
            //check if the thread exsists if it does abort the thread...
            if (m_ListenThread != null)
                m_ListenThread.Abort();
            //...and then reset it to null
            m_ListenThread = null;

            return;
        }
        //this is the method that will be called by the start thread
        private void StartListenLoop()
        {
            //might be a bit dirty but create an infinate loop in a seperate thread that will only end when the thread is killed... therfore it is crucial that stoplistening is called before the console appli
            //cation exits
            while (true)
            {
                //check if there is a key available and the 
                if (Console.KeyAvailable && m_KeyListeners.Count > 0)
                {
                    //read the key
                    ConsoleKeyInfo cki = Console.ReadKey(true);
                    //for every registered listener
                    for (int i = 0; i < m_KeyListeners.Count; i++)
                    {
                        //call the key event function
                        m_KeyListeners[i].KeyEvent(cki);
                    }
                }
            }
        }
        //create a list to store the key listeners
        private List<Interfaces.IKeyListener> m_KeyListeners = new List<Interfaces.IKeyListener>();
        //method to register a listener
        public bool RegisterListener(Interfaces.IKeyListener ikl)
        {
            //check if there are any listeners registered; if there is then check if this listener is already registered; if it is registered then return it as false
            if(m_KeyListeners.Count > 0)
            {
                for(int i = 0; i < m_KeyListeners.Count; i++)
                {
                    if (m_KeyListeners[i] == ikl)
                        return false;
                }
            }

            //the listener wasnt already registered so we need to add it to the listener list
            m_KeyListeners.Add(ikl);

            return true;
        }
        //a method to remove a listener so that they key event is no longer called
        public void DeregisterListener(Interfaces.IKeyListener ikl)
        {
            //check if there are any listeners registered
            if (m_KeyListeners.Count > 0)
            {
                //for every listener
                for (int i = 0; i < m_KeyListeners.Count; i++)
                {
                    //check if the iterate listener is the one you want to remove, if it is then remove it otherwise... well dont
                    if (m_KeyListeners[i] == ikl)
                    {
                        m_KeyListeners.RemoveAt(i);
                        return;
                    }
                }
            }

            return;
        }
    }
}
