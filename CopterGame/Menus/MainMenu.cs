﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame
{
    //this class is going to handle the main menu - going with a singleton as we only want one instance of the main menu
    class CMainMenu : Interfaces.IKeyListener
    {
        private CGeometry m_HeliMadnessText = new CGeometry(CGeometry.GetGeomDir() + "HeliMadness.geom");
        
        //this function is called when the new game menu item is selected
        private void NewGameAction()
        {
            CGame.Instance.PostMessage(CGame.GameMessages.NAME_SCREEN);
        }
        private void HighScoreAction()
        {
            this.m_bVisable = false;
            Screens.CHighScoreScreen.Instance.Visable = true;
        }

        //this function is called when the exit menu item is selected
        private void ExitAction()
        {
            CGame.Instance.PostMessage(CGame.GameMessages.EXIT);
            return;
        }

        //called by the key manager
        public void KeyEvent(ConsoleKeyInfo cki)
        {
            //only handle a key press if the menu is visable
            if (m_bVisable)
            {
                //switch statment for the key pressed
                switch (cki.Key)
                {
                    default:
                        break;

                    //when up arrow is pressed move between menu items
                    case ConsoleKey.UpArrow:
                    {
                        do
                        {
                            //if the new position in the item array will be less then zero, set it to the end of the list
                            if (m_iMenuItemSelected - 1 < 0)
                            {
                                m_MenuItems[m_iMenuItemSelected].Selected = false;
                                m_iMenuItemSelected = m_MenuItems.Count - 1;
                                m_MenuItems[m_iMenuItemSelected].Selected = true;
                                //redraw the menu
                                Redraw();
                                continue;
                            }

                            //otherwise just decrement the selected item and update the items
                            m_MenuItems[m_iMenuItemSelected].Selected = false;
                            m_iMenuItemSelected--;
                            m_MenuItems[m_iMenuItemSelected].Selected = true;
                            //redraw the menu
                            Redraw();

                        } while (m_MenuItems[m_iMenuItemSelected].Enabled == false);
                        
                        break;
                    }

                    //when down arrow is pressed move between menu items
                    case ConsoleKey.DownArrow:
                    {
                        do
                        {
                            //check if the next one is going to go past the end of the array then set it back to beginning
                            if (m_iMenuItemSelected + 1 > m_MenuItems.Count - 1)
                            {
                                //set the current iterate item selected to fales
                                m_MenuItems[m_iMenuItemSelected].Selected = false;
                                //reset item selected to begining
                                m_iMenuItemSelected = 0;
                                //set the new item selected, selected to true
                                m_MenuItems[m_iMenuItemSelected].Selected = true;
                                //redraw the menu
                                Redraw();
                                continue;
                            }

                            //otherwise just move it down
                            //set the currently selected item, selected to false
                            m_MenuItems[m_iMenuItemSelected].Selected = false;
                            //increment the item selected
                            m_iMenuItemSelected++;
                            //set the newly iterated item selected, selected to true
                            m_MenuItems[m_iMenuItemSelected].Selected = true;
                            //redraw the menu
                            Redraw();
                        } while (m_MenuItems[m_iMenuItemSelected].Enabled == false);
                        break;
                    }

                    //when enter key is pressed call the action for the menu item
                    case ConsoleKey.Enter:
                    {
                        //check if the iterated item has an action defined then calle it
                        if (m_MenuItems[m_iMenuItemSelected].Action != null)
                                m_MenuItems[m_iMenuItemSelected].Action();

                        break;
                    }
                }

                return;
            }

            return;
        }

        //using a singleton so we need to store the only instance of the class
        static private CMainMenu sm_Instance = null;
        //create a property to retrive the instance
        static public CMainMenu Instance
        {
            get
            {
                //check if the instance has been created
                if (sm_Instance == null)
                {
                    //if not create it and return it
                    sm_Instance = new CMainMenu();
                    return sm_Instance;
                }

                //has already been created so return it
                return sm_Instance;
            }
        }
        
        //store the index of the currently selected menu item
        private int m_iMenuItemSelected = 0;
        //create a list of the menu items
        private List<CMenuItem> m_MenuItems = new List<CMenuItem>();

        //make the constructor private as its a singleton
        private CMainMenu()
        {
            CKeyManager.Instance.RegisterListener(this);

            return;
        }

        //function to initialize the menu
        public void Initialize()
        {
            //calculate and store the x position of the menu
            int x = (int)(CCanvas.Instance.Width / 2) - ((int)(CCanvas.Instance.Width / 3) / 2);

            //create a menu item for each option and add them to the list /--/
            CMenuItem newGame = new CMenuItem(x, m_HeliMadnessText.Dimensions.Y * 2 + CCanvas.Instance.DrawYStart, "New Game", true);
            newGame.Selected = true;
            newGame.Action = NewGameAction;

            m_MenuItems.Add(newGame);

            CMenuItem highScores = new CMenuItem(x, newGame.Y + newGame.Height + 2, "High Scores", true);
            highScores.Action = HighScoreAction;

            m_MenuItems.Add(highScores);

            CMenuItem exit = new CMenuItem(x, highScores.Y + highScores.Height + 2, "Exit to Desktop", true);
            exit.Enabled = true;
            exit.Action = ExitAction;

            m_MenuItems.Add(exit);

            return;
        }

        //store whether or not the menu is visable
        private bool m_bVisable = false;
        public bool Visable
        {
            get
            {
                return m_bVisable;
            }
            set
            {
                //if the value set too is true
                if (value == true)
                {
                    //set visable var to true
                    m_bVisable = true;
                    //redraw the console
                    Redraw();
                }
                else
                {
                    //set visable var to false
                    m_bVisable = false;
                    //set state of currently selected menu item to not selected
                    m_MenuItems[m_iMenuItemSelected].Selected = false;
                    //reset the selected back to zero
                    m_iMenuItemSelected = 0;
                    //set state of newly selected menu item to selected
                    m_MenuItems[m_iMenuItemSelected].Selected = true;
                    //clear the menu of menu items to false
                    Clear();
                }
            }
        }

        public void Redraw()
        {
            //if the menu is visable the readraw all the items
            if (m_bVisable)
            {
                int xStart = (CCanvas.Instance.Width / 2) - (m_HeliMadnessText.Dimensions.X / 2);
                //going to reuse the geom code to load some ascii art for game over
                for (int y = 0; y < m_HeliMadnessText.Geometry.Count; y++)
                {
                    for (int x = 0; x < m_HeliMadnessText.Geometry[y].Count; x++)
                    {
                        CCanvas.Instance.SetCursorPosition(xStart + x, CCanvas.Instance.DrawYStart + y);
                        CCanvas.Instance.Write(m_HeliMadnessText.Geometry[y][x]);
                    }
                }

                CCanvas.Instance.PresentCanvas();

                for (int i = 0; i < m_MenuItems.Count; i++)
                    m_MenuItems[i].Redraw();
            }
        }

        public void Clear()
        {
            //just call the clear method for each item
            for (int i = 0; i < m_MenuItems.Count; i++)
                m_MenuItems[i].Clear();
        }
    }
}
