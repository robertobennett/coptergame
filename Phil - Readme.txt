Accessing Menus:
Up arrow - to move up the menu items
Down arrow - to move down the menu items
Enter - to select the hightlighted (green) menu item
Escape - to go back from a menu

Game Rules:
Points are awarded for every obsticle that reaches the end:
A single point is awarded for a box
Two points are awarded for a rocket
Fifteen points are awarded for a helicopter

Power Ups:
In the game there is a single power up which looks like:
XXXXXXXX
X      X
X  x2  X
X      X
XXXXXXXX
This instantly double the amount of points you have and is awarded when you crash into it

Game Controls:
To control the helicopter:
Up arrow - Move helicopter up
Down arrow - Move helicopter down
Right arrow - Move helicopter forward
Left arrow - Move helicopter backward