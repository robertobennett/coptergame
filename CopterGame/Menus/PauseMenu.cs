﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame
{
    //this class is going to handle the main menu - going with a singleton as we only want one instance of the main menu
    class CPauseMenu : Interfaces.IKeyListener
    {
        //this function is called when the new game menu item is selected
        private void ResumeGameAction()
        {
            CGame.Instance.PostMessage(CGame.GameMessages.UNPAUSE);
            return;
        }

        //this function is called when the exit menu item is selected
        private void ExitToMainMenuAction()
        {
            //CGame.Instance.PostMessage(CGame.GameMessages.UNPAUSE);
            CGame.Instance.PostMessage(CGame.GameMessages.RETURN_TO_MENU);
            return;
        }

        private void RestartGameAction()
        {
            CGame.Instance.PostMessage(CGame.GameMessages.RESTART_GAME);
            return;
        }

        //this function is called when the exit menu item is selected
        private void ExitAction()
        {
            CGame.Instance.PostMessage(CGame.GameMessages.EXIT);
            return;
        }

        //called by the key manager
        public void KeyEvent(ConsoleKeyInfo cki)
        {
            //only handle a key press if the menu is visable
            if (m_bVisable)
            {
                //switch statment for the key pressed
                switch (cki.Key)
                {
                    default:
                        break;

                    //when up arrow is pressed move between menu items
                    case ConsoleKey.UpArrow:
                    {
                        //if the new position in the item array will be less then zero, set it to the end of the list
                        if (m_iMenuItemSelected - 1 < 0)
                        {
                            m_MenuItems[m_iMenuItemSelected].Selected = false;
                            m_iMenuItemSelected = m_MenuItems.Count - 1;
                            m_MenuItems[m_iMenuItemSelected].Selected = true;
                            //redraw the menu
                            Redraw();
                            break;
                        }

                        //otherwise just decrement the selected item and update the items
                        m_MenuItems[m_iMenuItemSelected].Selected = false;
                        m_iMenuItemSelected--;
                        m_MenuItems[m_iMenuItemSelected].Selected = true;
                        //redraw the menu
                        Redraw();

                        break;
                    }

                    //when down arrow is pressed move between menu items
                    case ConsoleKey.DownArrow:
                    {
                        //check if the next one is going to go past the end of the array then set it back to beginning
                        if (m_iMenuItemSelected + 1 > m_MenuItems.Count - 1)
                        {
                            //set the current iterate item selected to fales
                            m_MenuItems[m_iMenuItemSelected].Selected = false;
                            //reset item selected to begining
                            m_iMenuItemSelected = 0;
                            //set the new item selected, selected to true
                            m_MenuItems[m_iMenuItemSelected].Selected = true;
                            //redraw the menu
                            Redraw();
                            break;
                        }

                        //otherwise just move it down
                        //set the currently selected item, selected to false
                        m_MenuItems[m_iMenuItemSelected].Selected = false;
                        //increment the item selected
                        m_iMenuItemSelected++;
                        //set the newly iterated item selected, selected to true
                        m_MenuItems[m_iMenuItemSelected].Selected = true;
                        //redraw the menu
                        Redraw();
                        break;
                    }

                    //when enter key is pressed call the action for the menu item
                    case ConsoleKey.Enter:
                    {
                        //check if the iterated item has an action defined then calle it
                        if (m_MenuItems[m_iMenuItemSelected].Action != null)
                                m_MenuItems[m_iMenuItemSelected].Action();

                        break;
                    }
                }

                return;
            }

            return;
        }

        //using a singleton so we need to store the only instance of the class
        static private CPauseMenu sm_Instance = null;
        //create a property to retrive the instance
        static public CPauseMenu Instance
        {
            get
            {
                //check if the instance has been created
                if (sm_Instance == null)
                {
                    //if not create it and return it
                    sm_Instance = new CPauseMenu();
                    return sm_Instance;
                }

                //has already been created so return it
                return sm_Instance;
            }
        }
        
        //store the index of the currently selected menu item
        private int m_iMenuItemSelected = 0;
        //create a list of the menu items
        private List<CMenuItem> m_MenuItems = new List<CMenuItem>();

        //make the constructor private as its a singleton
        private CPauseMenu()
        {
            CKeyManager.Instance.RegisterListener(this);

            return;
        }

        //function to initialize the menu
        public void Initialize()
        {
            //calculate and store the x position of the menu
            int x = (int)(CCanvas.Instance.Width / 2) - ((int)(CCanvas.Instance.Width / 3) / 2);

            //create a menu item for each option and add them to the list /--/
            CMenuItem resGame = new CMenuItem(x, CCanvas.Instance.DrawYStart + 2, "Resume Game", true);
            resGame.Selected = true;
            resGame.Action = ResumeGameAction;

            m_MenuItems.Add(resGame);

            //create a menu item for each option and add them to the list /--/
            CMenuItem restartGame = new CMenuItem(x, resGame.Y + 2 + resGame.Height, "Restart Game", true);
            restartGame.Selected = false;
            restartGame.Action = RestartGameAction;

            m_MenuItems.Add(restartGame);

            CMenuItem exitToMenu = new CMenuItem(x, restartGame.Y + restartGame.Height + 2, "Return To Menu", true);
            exitToMenu.Action = ExitToMainMenuAction;

            m_MenuItems.Add(exitToMenu);

            CMenuItem exit = new CMenuItem(x, exitToMenu.Y + exitToMenu.Height + 2, "Exit to Desktop", true);
            exit.Action = ExitAction;

            m_MenuItems.Add(exit);

            return;
        }

        //store whether or not the menu is visable
        private bool m_bVisable = false;
        public bool Visable
        {
            get
            {
                return m_bVisable;
            }
            set
            {
                //if the value set too is true
                if (value == true)
                {
                    //set visable var to true
                    m_bVisable = true;
                    //redraw the console
                    Redraw();
                }
                else
                {
                    //set visable var to false
                    m_bVisable = false;
                    //set state of currently selected menu item to not selected
                    m_MenuItems[m_iMenuItemSelected].Selected = false;
                    //reset the selected back to zero
                    m_iMenuItemSelected = 0;
                    //set state of newly selected menu item to selected
                    m_MenuItems[m_iMenuItemSelected].Selected = true;
                    //clear the menu of menu items to false
                    Clear();
                }
            }
        }

        public void Redraw()
        {
            //if the menu is visable the readraw all the items
            if (m_bVisable)
            {
                for (int i = 0; i < m_MenuItems.Count; i++)
                    m_MenuItems[i].Redraw();
            }
        }

        public void Clear()
        {
            //just call the clear method for each item
            for (int i = 0; i < m_MenuItems.Count; i++)
                m_MenuItems[i].Clear();
        }
    }
}
