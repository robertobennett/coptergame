﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace CopterGame
{
    //this class is responsable for controling the execution of the entier game from menus to the actual game
    class CGame : Interfaces.IListener, Interfaces.IKeyListener
    {
        //called when a game message is dispatced
        public void Message(GameMessages msg)
        {
            //switch the message
            switch (msg)
            {
                default:
                    break;

                //this is sent when the name screen is requsted to be displayed when the new game menu item is selected
                case GameMessages.NAME_SCREEN:
                    //hide the main menu
                    CMainMenu.Instance.Visable = false;
                    //clear the main menu
                    CMainMenu.Instance.Clear();
                    //show the namescree and update the game state
                    Screens.CNameScreen.Instance.Visable = true;
                    m_State = GameStates.NAME_SCREEN;
                    break;
                    
                //this is sent when the player has entered their name
                case GameMessages.NEW_GMAE:
                    //update the state
                    m_State = GameStates.INITIALIZING;
                    //hide and clear the name screen
                    Screens.CNameScreen.Instance.Visable = false;
                    Screens.CNameScreen.Instance.Clear();
                    //set the player name according to what the player entered
                    m_sPlayerName = Screens.CNameScreen.Instance.PlayerName;
                    //call the new game function
                    this.NewGame();
                    //update the state again
                    m_State = GameStates.GAME_RUNNING; 
                    break;

                //sent when the player presses escape during the game running state
                case GameMessages.PAUSE:
                    //update state first so everything else stops
                    m_State = GameStates.PAUSED;
                    //clear the canvas
                    CCanvas.Instance.Clear(false, false);
                    //present the blank canvas
                    CCanvas.Instance.PresentCanvas();
                    //show the pause menu
                    CPauseMenu.Instance.Visable = true;
                    break;

                //sent when the player resumes the game
                case GameMessages.UNPAUSE:
                    //clear the canvas and present it
                    CCanvas.Instance.Clear();
                    CCanvas.Instance.PresentCanvas();
                    //hide the pause menu
                    CPauseMenu.Instance.Visable = false;
                    //update the state
                    m_State = GameStates.GAME_RUNNING;
                    break;

                //Called when the player selects the restart game menu item
                case GameMessages.RESTART_GAME:
                    //update the state
                    m_State = GameStates.INITIALIZING;
                    //hide the pause menu
                    CPauseMenu.Instance.Visable = false;
                    //clear and present the canvas
                    CCanvas.Instance.Clear();
                    CCanvas.Instance.PresentCanvas();
                    //end the current game and then create a new game
                    EndGame();
                    NewGame();
                    //update the state
                    m_State = GameStates.GAME_RUNNING;
                    break;

                //sent when the user selects return to menu from the pause menu
                case GameMessages.RETURN_TO_MENU:
                    //end the current game
                    EndGame();
                    //hide the pause menu
                    CPauseMenu.Instance.Visable = false;
                    //hide the game over screen as the player doesnt need to see it
                    Screens.CGameOverScreen.Instance.Visable = false;
                    CCanvas.Instance.Clear(false, false);
                    //clear the canvas to remove the border ect and present it
                    CCanvas.Instance.PresentCanvas();
                    //show the main menu and update the state
                    CMainMenu.Instance.Visable = true;
                    m_State = GameStates.MAIN_MENU;
                    break;

                //sent when the player dies
                case GameMessages.GAME_OVER:
                    //update the state so all stops
                    m_State = GameStates.GAME_OVER;
                    //end the current game
                    EndGame();
                    //clear the canvas and remove border then present it
                    CCanvas.Instance.Clear(false, false);
                    CCanvas.Instance.PresentCanvas();
                    //show the game over screen
                    Screens.CGameOverScreen.Instance.Visable = true;
                    break;
            }
        }

        
        static CCanvas sm_CanvasInstance = CCanvas.Instance;
        //an enum of all possible game messages
        public enum GameMessages
        {
            NO_MESSAGES = 0,
            EXIT = 1,
            NEW_GMAE = 2,
            PAUSE = 3,
            UNPAUSE = 4,
            RETURN_TO_MENU = 5,
            GAME_OVER = 6,
            NAME_SCREEN = 8,
            RESTART_GAME = 7
        }

        //an enum of all possible states that the game can be in
        public enum GameStates
        {
            UNKNOWN = 0,
            INITIALIZING = 1,
            MAIN_MENU = 2,
            NAME_SCREEN = 6,
            GAME_RUNNING = 3,
            PAUSED = 4,
            GAME_OVER = 5
        }

        //store class instance privatly and create a property to access it
        private static CGame sm_Instance = null;
        public static CGame Instance
        {
            get
            {
                //if the isntance is null then create an instance an return it
                if(sm_Instance == null)
                {
                    sm_Instance = new CGame();
                    return sm_Instance;
                }
                //instance exsists so return it
                return sm_Instance;
            }
        }
        //hide constructor as its a singleton
        private CGame()
        {
            RegisterListener(this);
            CKeyManager.Instance.RegisterListener(this);
            return;
        }
        //struct to define what is stored/displayed for high score, links player name and score
        public struct HighScoreEntry
        {
            public String sName;
            public int iScore;
        }
        //player name initialize it to something it wouldnt be by default ie "AAA"
        String m_sPlayerName = "ZZZ";
        //list to keep the top three scores and property to retrive the scores
        private List<HighScoreEntry> m_Scores = new List<HighScoreEntry>();
        public List<HighScoreEntry> Scores
        {
            get
            {
                return m_Scores;
            }
        }
        //function to read top three scores from file
        public void ReadHighScores()
        {
            //clear the scores list as these are old scores now
            m_Scores.Clear();
            //attempt to read from file
            StreamReader scoresFile = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "/HighScores.txt");
            //list to store read lines
            List<String> lines = new List<String>();
            //while not at end off file read from it
            while (scoresFile.EndOfStream != true)
            {
                lines.Add(scoresFile.ReadLine());
            }
            //close the file all lines are now read
            scoresFile.Close();
            //only reading top 3 scores if more are stored which they shouldnt be throw an exception
            if (lines.Count <= 3)
            {
                //for all of the lines read split them and store in the HighScoreEntry struct
                for (int i = 0; i < lines.Count; i++)
                {
                    HighScoreEntry hse = new HighScoreEntry();
                    //used to start the sub strings from the split
                    string[] subs = lines[i].Split('/');
                    if (subs.Length == 2)
                    {
                        hse.sName = subs[0];
                        hse.iScore = int.Parse(subs[1]);
                        m_Scores.Add(hse);
                    }
                }
            }
            else
            {
                throw new Exception("More than 3 entries");
            }

            return;
        }
        //property to retrive the players score
        public int Score
        {
            get
            {
                return m_iScore;
            }
        }
        //function to try and add a new highscore
        private bool TryAddNewHighScore(int iScore)
        {
            //if the score is greater then zero then it can be added
            if (iScore > 0 && !m_bScoreAdded)
            {
                //if there are no scores saved then add it and return true
                if (m_Scores.Count == 0)
                {
                    HighScoreEntry hse = new HighScoreEntry();
                    hse.iScore = iScore;
                    hse.sName = m_sPlayerName;

                    m_Scores.Add(hse);

                    m_bScoreAdded = true;
                    return true;
                }
                else
                {
                    //other wise we need to check where to instert it
                    HighScoreEntry hse = new HighScoreEntry();
                    hse.iScore = iScore;
                    hse.sName = m_sPlayerName;
                    
                    //check if it is greater then any of the scores and if it is insert it
                    if(iScore > m_Scores[0].iScore)
                    {
                        m_Scores.Insert(0, hse);
                    }
                    else if (m_Scores.Count >= 2 && iScore > m_Scores[1].iScore)
                    {
                        m_Scores.Insert(1, hse);
                    }
                    else if (m_Scores.Count >= 3 && iScore > m_Scores[2].iScore)
                    {
                        m_Scores.Insert(2, hse);
                    }
                    else
                    {
                        m_Scores.Insert(m_Scores.Count, hse);
                    }
                    //then check if there are four scores stored, if there is remove the last score which will also be the lowest
                    if (m_Scores.Count == 4)
                    {
                        m_Scores.RemoveAt(m_Scores.Count - 1);
                    }

                    m_bScoreAdded = true;
                    return true;
                }
            }

            return false;
        }

        //function to write the highscores to the file
        private void WriteHighScores()
        {
            StreamWriter scoresFile = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "/HighScores.txt");
            scoresFile.Flush();
            for(int i = 0; i < m_Scores.Count; i++)
            {
                scoresFile.WriteLine(m_Scores[i].sName + "/" + m_Scores[i].iScore);
            }
            scoresFile.Close();

            return;
        }
        //this function will initialize all objects needed
        //intiailie all menus then load the main menu and read the high scores
        public void Initialize() 
        {
            m_State = GameStates.INITIALIZING;

            CPauseMenu.Instance.Initialize();
            Screens.CGameOverScreen.Instance.Initialize();
            Screens.CNameScreen.Instance.Initialize();
            Screens.CHighScoreScreen.Instance.Initialize();
            CMainMenu.Instance.Initialize();
            CMainMenu.Instance.Visable = true;
            ReadHighScores();

            m_State = GameStates.MAIN_MENU;

            return;
        }
        //function used by external classes to add to the players score
        public void AddScore(int i)
        {
            m_iScore += i;
            if (m_iScore < 0)
                m_iScore = 0;

            return;
        }
        //store the instance of the player entity to allow other classes to retrive properties about the player
        private Entities.CPlayer m_Player = null;
        //property to get the players position
        public Math.Vector2D PlayerPos
        {
            get
            {
                return m_Player.Pos;
            }
        }
        //var to store the current score of the player
        private int m_iScore = 0;
        private bool m_bScoreAdded = false;
        //function to create a new game
        private void NewGame()
        {
            //create the player entitly
            m_Player = CEntityManager.Instance.CreateEntity<Entities.CPlayer>();
            //set the player pos to be the center of the map
            m_Player.Pos = new Math.Vector2D(CCanvas.Instance.DrawXStart + 2, ((CCanvas.Instance.DrawYEnd - CCanvas.Instance.DrawYStart) / 2));
            //call reset on the map so it creates a new seed
            Managers.Secondary.CMap.Instance.Reset();//reset the score to zero
            m_iScore = 0;
            m_bScoreAdded = false;

            return;
        }
        //function to clean up when the game ends
        private void EndGame()
        {
            //destroy all entities as game has ended
            CEntityManager.Instance.DestroyAll();
            //try and add the new score if a new score was added then update the file
            if (TryAddNewHighScore(m_iScore))
                WriteHighScores();

            return;
        }

        //this function will update renderer, physics and entities
        public void Update()
        {
            //check if the game state is running if it is then do all the updates
            if (State == GameStates.GAME_RUNNING)
            {
                CRenderer.Instance.PreDraw();
                Managers.Secondary.CMap.Instance.Update();
                CEntityManager.Instance.Update();
                CPhysics.Instance.Update();
                CRenderer.Instance.Draw();
                CCanvas.Instance.Hint = "Score: " + m_iScore;
            }

            return;
        }

        //call this to completly shutdown all the game components
        public void Shutdown()
        {
            CKeyManager.Instance.DeregisterListener(this);
            DeregisterListener(this);
            sm_CanvasInstance.Shutdown();
            return;
        }

        //called when a key is pressed
        public void KeyEvent(ConsoleKeyInfo cki)
        {
            switch (cki.Key)
            {
                default:
                    break;

                case ConsoleKey.Escape:
                {
                        //if the game is running and escape is pressed then post the pause message
                    if (this.m_State == GameStates.GAME_RUNNING)
                        this.PostMessage(GameMessages.PAUSE);

                    break;
                }
            }

            return;
        }

        //store a list of all the listeners for game messages
        private List<Interfaces.IListener> m_Listeners = new List<Interfaces.IListener>();
        //a function to register the listeners
        public bool RegisterListener(Interfaces.IListener lst)
        {
            //check if this listener is already registered
            if(m_Listeners.Count > 0)
            {
                for(int i = 0; i < m_Listeners.Count; i++)
                {
                    if (lst == m_Listeners[i])
                        return false;
                }
            }

            m_Listeners.Add(lst);

            return true;
        }
        //a function to remove listeners
        public void DeregisterListener(Interfaces.IListener lst)
        {
            //find it and remove it
            if (m_Listeners.Count > 0)
            {
                for (int i = 0; i < m_Listeners.Count; i++)
                {
                    if (lst == m_Listeners[i])
                    {
                        m_Listeners.RemoveAt(i);
                        return;
                    }
                }
            }

            return;
        }
        //a function to dispatch messages to the listeners
        public void DispatchMessage(GameMessages msg)
        {
            //just iterate through all listeners
            if (m_Listeners.Count > 0)
            {
                for (int i = 0; i < m_Listeners.Count; i++)
                {
                    m_Listeners[i].Message(msg);
                }
            }

            return;
        }
        //a function to push a message to the message que
        public void PostMessage(GameMessages msg)
        {
            //add message to the que if the message passed is not NO_MESSAGE
            if(msg != GameMessages.NO_MESSAGES)
            {
                m_MessageQue.Add(msg);
                return;
            }

            return;
        }
        //create a list to store the messages that are qued
        private List<GameMessages> m_MessageQue = new List<GameMessages>();
        //a function to retrive the next last message in the que
        public GameMessages PeekMessage(bool bRemove = false)
        {
            //check if there are messages qued
            if(m_MessageQue.Count > 0)
            {
                //if the remove flag is passed then
                if (bRemove)
                {
                    //store the first message
                    GameMessages msg = m_MessageQue[0];
                    //remove it
                    m_MessageQue.RemoveAt(0);
                    //return it
                    return msg;
                }
                //otherwise just return it
                else
                {
                    return m_MessageQue[0];
                }
            }

            //if there were no messages in the que just return no message
            return GameMessages.NO_MESSAGES;
        }
        //store the state of the game
        private GameStates m_State = GameStates.UNKNOWN;
        //create access to retrive the state
        public GameStates State
        {
            get
            {
                return m_State;
            }
        }
    }
}