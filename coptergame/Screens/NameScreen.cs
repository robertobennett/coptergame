﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Screens
{
    //this class is going to be the name screen and will allow the user the enter a three character abreviation or not abreviation of their name
    class CNameScreen: Interfaces.IKeyListener
    {
        //create "fonts" from the font files, load them using the geometry loader - static as they only need to be created once as they can be drawn multiple times with only one instance
        private static CGeometry m_A = new CGeometry(CGeometry.GetGeomDir() + "A.font");
        private static CGeometry m_B = new CGeometry(CGeometry.GetGeomDir() + "B.font");
        private static CGeometry m_C = new CGeometry(CGeometry.GetGeomDir() + "C.font");
        private static CGeometry m_D = new CGeometry(CGeometry.GetGeomDir() + "D.font");
        private static CGeometry m_E = new CGeometry(CGeometry.GetGeomDir() + "E.font");
        private static CGeometry m_F = new CGeometry(CGeometry.GetGeomDir() + "F.font");
        private static CGeometry m_G = new CGeometry(CGeometry.GetGeomDir() + "G.font");
        private static CGeometry m_H = new CGeometry(CGeometry.GetGeomDir() + "H.font");
        private static CGeometry m_I = new CGeometry(CGeometry.GetGeomDir() + "I.font");
        private static CGeometry m_J = new CGeometry(CGeometry.GetGeomDir() + "J.font");
        private static CGeometry m_K = new CGeometry(CGeometry.GetGeomDir() + "K.font");
        private static CGeometry m_L = new CGeometry(CGeometry.GetGeomDir() + "L.font");
        private static CGeometry m_M = new CGeometry(CGeometry.GetGeomDir() + "M.font");
        private static CGeometry m_N = new CGeometry(CGeometry.GetGeomDir() + "N.font");
        private static CGeometry m_O = new CGeometry(CGeometry.GetGeomDir() + "O.font");
        private static CGeometry m_P = new CGeometry(CGeometry.GetGeomDir() + "P.font");
        private static CGeometry m_Q = new CGeometry(CGeometry.GetGeomDir() + "Q.font");
        private static CGeometry m_R = new CGeometry(CGeometry.GetGeomDir() + "R.font");
        private static CGeometry m_S = new CGeometry(CGeometry.GetGeomDir() + "S.font");
        private static CGeometry m_T = new CGeometry(CGeometry.GetGeomDir() + "T.font");
        private static CGeometry m_U = new CGeometry(CGeometry.GetGeomDir() + "U.font");
        private static CGeometry m_V = new CGeometry(CGeometry.GetGeomDir() + "V.font");
        private static CGeometry m_W = new CGeometry(CGeometry.GetGeomDir() + "W.font");
        private static CGeometry m_X = new CGeometry(CGeometry.GetGeomDir() + "X.font");
        private static CGeometry m_Y = new CGeometry(CGeometry.GetGeomDir() + "Y.font");
        private static CGeometry m_Z = new CGeometry(CGeometry.GetGeomDir() + "Z.font");
        //legacy code, is possible to use the font manager but no point in chaning it now (font manager was added afterwards)
        private static CGeometry m_PleaseEnterYourName = new CGeometry(CGeometry.GetGeomDir() + "Please Enter Your Name.font");

        //a list that contains handles to all the geometrys for all the letters *1
        private List<CGeometry> m_CharGeomList = new List<CGeometry>();
        //a list that contains all of the literal character ie 'a' *1
        //*1 - this is so that the correct character geometry can be accessed as well as the correct literal character using an index
        private List<String> m_Chars = new List<String>();
        //this stores which character the user is changing
        private int m_iSelection = 0;
        //this stores the index of the first character of the players name
        private int m_iFirstChar = 0;
        //this stores the index of the second character of the players name
        private int m_iSecondChar = 0;
        //this stores the index of the third character of the players name
        private int m_iThirdChar = 0;
        //this stores the player name as a string and is set when the user presses enter
        private String m_sPlayerName = "";
        //this property allows acess to the players name
        public String PlayerName
        {
            get
            {
                return m_sPlayerName;
            }
        }
        //called by the key manager
        public void KeyEvent(ConsoleKeyInfo cki)
        {
            //only handle a key press if the menu is visable
            if (m_bVisable)
            {
                //switch statment for the key pressed
                switch (cki.Key)
                {
                    default:
                        break;

                    //when up arrow is pressed move between menu items
                    case ConsoleKey.LeftArrow:
                        {
                            m_iSelection--;
                            //if goes lower then first item index loop rouynd to the last item
                            if (m_iSelection < 0)
                                m_iSelection = 2;

                            //clear and redraw window
                            Clear();
                            Redraw();

                            break;
                        }

                    //when down arrow is pressed move between menu items
                    case ConsoleKey.RightArrow:
                        {
                            m_iSelection++;
                            //if goes over the last index loop to first
                            if (m_iSelection > 2)
                                m_iSelection = 0;

                            //clear and redraw window
                            Clear();
                            Redraw();

                            break;
                        }

                    //when up arrow is pressed move between menu items
                    case ConsoleKey.UpArrow:
                        {
                            //check which character the user is editing
                            //only commenting one of the if statements as the other works the same
                            //check if the first character is selected
                            if (m_iSelection == 0)
                            {
                                //lower the index
                                m_iFirstChar--;

                                //if the index is below first then loop to last
                                if (m_iFirstChar < 0)
                                    m_iFirstChar = 25;
                            }
                            else if (m_iSelection == 1)
                            {
                                m_iSecondChar--;

                                if (m_iSecondChar < 0)
                                    m_iSecondChar = 25;
                            }
                            else if (m_iSelection == 2)
                            {
                                m_iThirdChar--;

                                if (m_iThirdChar < 0)
                                    m_iThirdChar = 25;
                            }

                            //clear and redraw window
                            Clear();
                            Redraw();

                            break;
                        }

                    //when down arrow is pressed move between menu items
                    case ConsoleKey.DownArrow:
                        {
                            //check which character the user is editing
                            //only commenting one of the if statements as the other works the same
                            //check if the first character is selected
                            if (m_iSelection == 0)
                            {
                                //increase the index
                                m_iFirstChar++;
                                //if the index is above last then loop to first
                                if (m_iFirstChar > 25)
                                    m_iFirstChar = 0;
                            }
                            else if (m_iSelection == 1)
                            {
                                m_iSecondChar++;
                                if (m_iSecondChar > 25)
                                    m_iSecondChar = 0;
                            }
                            else if (m_iSelection == 2)
                            {
                                m_iThirdChar++;
                                if (m_iThirdChar > 25)
                                    m_iThirdChar = 0;
                            }

                            //clear and redraw window
                            Clear();
                            Redraw();

                            break;
                        }

                    //when enter key is pressed call the action for the menu item
                    case ConsoleKey.Enter:
                        {
                            //when the enter key is pressed then store the three chars as player name in the string
                            m_sPlayerName = m_Chars[m_iFirstChar] + m_Chars[m_iSecondChar] + m_Chars[m_iThirdChar];
                            //as this screen is displayed pre to new game when the user has entered their name we need to sent the new game message
                            CGame.Instance.DispatchMessage(CGame.GameMessages.NEW_GMAE);
                            break;
                        }
                }

                return;
            }

            return;
        }

        //using a singleton so we need to store the only instance of the class
        static private CNameScreen sm_Instance = null;
        //create a property to retrive the instance
        static public CNameScreen Instance
        {
            get
            {
                //check if the instance has been created
                if (sm_Instance == null)
                {
                    //if not create it and return it
                    sm_Instance = new CNameScreen();
                    return sm_Instance;
                }

                //has already been created so return it
                return sm_Instance;
            }
        }

        //make the constructor private as its a singleton
        private CNameScreen()
        {
            CKeyManager.Instance.RegisterListener(this);

            return;
        }

        //function to initialize the menu
        public void Initialize()
        {
            //start of hidden
            m_bVisable = false;

            //initialise the lists
            //add all the geom for the characters
            m_CharGeomList.Add(m_A);
            m_CharGeomList.Add(m_B);
            m_CharGeomList.Add(m_C);
            m_CharGeomList.Add(m_D);
            m_CharGeomList.Add(m_E);
            m_CharGeomList.Add(m_F);
            m_CharGeomList.Add(m_G);
            m_CharGeomList.Add(m_H);
            m_CharGeomList.Add(m_I);
            m_CharGeomList.Add(m_J);
            m_CharGeomList.Add(m_K);
            m_CharGeomList.Add(m_L);
            m_CharGeomList.Add(m_M);
            m_CharGeomList.Add(m_N);
            m_CharGeomList.Add(m_O);
            m_CharGeomList.Add(m_P);
            m_CharGeomList.Add(m_Q);
            m_CharGeomList.Add(m_R);
            m_CharGeomList.Add(m_S);
            m_CharGeomList.Add(m_T);
            m_CharGeomList.Add(m_U);
            m_CharGeomList.Add(m_V);
            m_CharGeomList.Add(m_W);
            m_CharGeomList.Add(m_X);
            m_CharGeomList.Add(m_Y);
            m_CharGeomList.Add(m_Z);

            //add all the literal characters for the characters
            m_Chars.Add("A");
            m_Chars.Add("B");
            m_Chars.Add("C");
            m_Chars.Add("D");
            m_Chars.Add("E");
            m_Chars.Add("F");
            m_Chars.Add("G");
            m_Chars.Add("H");
            m_Chars.Add("I");
            m_Chars.Add("J");
            m_Chars.Add("K");
            m_Chars.Add("L");
            m_Chars.Add("M");
            m_Chars.Add("N");
            m_Chars.Add("O");
            m_Chars.Add("P");
            m_Chars.Add("Q");
            m_Chars.Add("R");
            m_Chars.Add("S");
            m_Chars.Add("T");
            m_Chars.Add("U");
            m_Chars.Add("V");
            m_Chars.Add("W");
            m_Chars.Add("X");
            m_Chars.Add("Y");
            m_Chars.Add("Z");

            return;
        }

        //store whether or not the menu is visable
        private bool m_bVisable = false;
        //propetry to set and get the visibility
        public bool Visable
        {
            get
            {
                return m_bVisable;
            }
            set
            {
                //if the value set too is true
                if (value == true)
                {
                    //set visable to true as the menu will now be displayed
                    m_bVisable = true;

                    //Clear the console to make sure there is nothing else there
                    Console.Clear();

                    //This only needs drawing once so placed the screen title draw code here
                    //work out xstart pos based on the center minus half the width of the text so the text is centralized
                    int xStart = CCanvas.Instance.DrawXStart + (CCanvas.Instance.Width / 2) - m_PleaseEnterYourName.Dimensions.X / 2;
                    //work out the ystart pos based on the center with an offset that is asthetically pleasing
                    int yStart = CCanvas.Instance.DrawYStart + (CCanvas.Instance.Height / 2) - 14;

                    //for every row of characters in the text
                    for (int y = 0; y < m_PleaseEnterYourName.Geometry.Count; y++)
                    {
                        //iterate each character
                        for (int x = 0; x < m_PleaseEnterYourName.Geometry[y].Count; x++)
                        {
                            //set the console position to the iterate char pos plus the start pos
                            Console.SetCursorPosition(x + xStart, y + yStart);
                            //write the iterated character
                            Console.Write(m_PleaseEnterYourName.Geometry[y][x]);
                        }
                    }

                    //reset selected char to the first index
                    m_iSelection = 0;
                    //clear the screen and draw the menu
                    Redraw();
                }
                else
                {
                    //set visable var to false
                    m_bVisable = false;
                    //reset the console color to the default
                    Console.ForegroundColor = ConsoleColor.Gray;
                    //clear the screen
                    Console.Clear();
                }
            }
        }

        //this is shared between redraw and clear methods and is a constant
        private const int spacing = 2;
        //this method writes directly to the console rather than with my canvas class as the game state is not running so the present method of the canvas wont be drawn and I need acess to the console colors which my canvas class doesnt support
        public void Redraw()
        {
            //if the menu is visable then readraw all the items
            if (m_bVisable)
            {
                //calculate the intial x and y positions for drawing
                //xStart consists of:
                //The CCanvas.Instance.DrawXStart because we are using the canvas width to work out the center we need to take into account the x start
                //CCanvas.Instance.DrawXStart + (CCanvas.Instance.Width / 2) - add on half the width to find the center point
                // - ((m_CharGeomList[m_iFirstChar].Dimensions.X + m_CharGeomList[m_iSecondChar].Dimensions.X + m_CharGeomList[m_iThirdChar].Dimensions.X + (2 * spacing)) / 2)- subtract the width of all characters with the spacing divided by two as half wants
                //to be left of center and other half right of center
                int xStart = CCanvas.Instance.DrawXStart + (CCanvas.Instance.Width / 2) - ((m_CharGeomList[m_iFirstChar].Dimensions.X + m_CharGeomList[m_iSecondChar].Dimensions.X + m_CharGeomList[m_iThirdChar].Dimensions.X + (2 * spacing)) / 2);
                //yStart consists of calculating mid point and offseting it which is hardcoded to something asthetically pleasing
                int yStart = CCanvas.Instance.DrawYStart + (CCanvas.Instance.Height / 2) - 4;

                //for each row in the first character
                for (int y = 0; y < m_CharGeomList[m_iFirstChar].Geometry.Count; y++)
                {
                    //for each character in the iterated row
                    for (int x = 0; x < m_CharGeomList[m_iFirstChar].Geometry[y].Count; x++)
                    {
                        //if the first character is currently selected then set it to green otherwise set it to default color
                        if (m_iSelection == 0)
                            Console.ForegroundColor = ConsoleColor.Green;
                        else
                            Console.ForegroundColor = ConsoleColor.Gray;

                        //set the cursor pos to the xstart pos and take into account the iterated character pos, so if it is drawing the third character along the fourth row this needs to be added to start positions for the x and y
                        Console.SetCursorPosition(x + xStart, y + yStart);
                        //write the iterated character
                        Console.Write(m_CharGeomList[m_iFirstChar].Geometry[y][x]);
                    }
                }

                //update the x start pos to take into account the width of the first character and add some spacing
                xStart += m_CharGeomList[m_iFirstChar].Dimensions.X + spacing;

                //for each row in the second character
                for (int y = 0; y < m_CharGeomList[m_iSecondChar].Geometry.Count; y++)
                {
                    //for each character in the iterated row
                    for (int x = 0; x < m_CharGeomList[m_iSecondChar].Geometry[y].Count; x++)
                    {
                        //if the second character is currently selected then set it to green otherwise set it to default color
                        if (m_iSelection == 1)
                            Console.ForegroundColor = ConsoleColor.Green;
                        else
                            Console.ForegroundColor = ConsoleColor.Gray;

                        //set the cursor pos to the xstart pos and take into account the iterated character pos, so if it is drawing the third character along the fourth row this needs to be added to start positions for the x and y
                        Console.SetCursorPosition(x + xStart, y + yStart);
                        //write the iterated character
                        Console.Write(m_CharGeomList[m_iSecondChar].Geometry[y][x]);
                    }
                }

                //update the x start pos to take into account the width of the second character and add some spacing
                xStart += m_CharGeomList[m_iFirstChar].Dimensions.X + spacing;

                //for each row in the third character
                for (int y = 0; y < m_CharGeomList[m_iThirdChar].Geometry.Count; y++)
                {
                    //for each character in the iterated row
                    for (int x = 0; x < m_CharGeomList[m_iThirdChar].Geometry[y].Count; x++)
                    {
                        //if the third character is currently selected then set it to green otherwise set it to default color
                        if (m_iSelection == 2)
                            Console.ForegroundColor = ConsoleColor.Green;
                        else
                            Console.ForegroundColor = ConsoleColor.Gray;

                        //set the cursor pos to the xstart pos and take into account the iterated character pos, so if it is drawing the third character along the fourth row this needs to be added to start positions for the x and y
                        Console.SetCursorPosition(x + xStart, y + yStart);
                        //write the iterated character
                        Console.Write(m_CharGeomList[m_iThirdChar].Geometry[y][x]);
                    }
                }

                //reset the console color to its default gray
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        public void Clear()
        {
            //calculate the intial x and y positions that were used for drawing to allow us to clear it
            //xStart consists of:
            //The CCanvas.Instance.DrawXStart because we are using the canvas width to work out the center we need to take into account the x start
            //CCanvas.Instance.DrawXStart + (CCanvas.Instance.Width / 2) - add on half the width to find the center point
            // - ((m_CharGeomList[m_iFirstChar].Dimensions.X + m_CharGeomList[m_iSecondChar].Dimensions.X + m_CharGeomList[m_iThirdChar].Dimensions.X + (2 * spacing)) / 2)- subtract the width of all characters with the spacing divided by two as half wants
            //to be left of center and other half right of center
            int xStart = CCanvas.Instance.DrawXStart + (CCanvas.Instance.Width / 2) - ((m_CharGeomList[m_iFirstChar].Dimensions.X + m_CharGeomList[m_iSecondChar].Dimensions.X + m_CharGeomList[m_iThirdChar].Dimensions.X + (2 * spacing)) / 2);
            //yStart consists of calculating mid point and offseting it which is hardcoded to something asthetically pleasing 
            int yStart = CCanvas.Instance.DrawYStart + (CCanvas.Instance.Height / 2) - 4;

            //for each row in the first character
            for (int y = 0; y < m_CharGeomList[m_iFirstChar].Geometry.Count; y++)
            {
                //for each character in the iterated row
                for (int x = 0; x < m_CharGeomList[m_iFirstChar].Geometry[y].Count; x++)
                {
                    //set the cursor pos to the xstart pos and take into account the iterated character pos, so if it is drawing the third character along the fourth row this needs to be added to start positions for the x and y
                    Console.SetCursorPosition(x + xStart, y + yStart);
                    //write a blank character
                    Console.Write(' ');
                }
            }

            //update the x start pos to take into account the width of the first character and add some spacing
            xStart += m_CharGeomList[m_iFirstChar].Dimensions.X + spacing;

            //for each row in the second character
            for (int y = 0; y < m_CharGeomList[m_iFirstChar].Geometry.Count; y++)
            {
                //for each character in the iterated row
                for (int x = 0; x < m_CharGeomList[m_iFirstChar].Geometry[y].Count; x++)
                {
                    //set the cursor pos to the xstart pos and take into account the iterated character pos, so if it is drawing the third character along the fourth row this needs to be added to start positions for the x and y
                    Console.SetCursorPosition(x + xStart, y + yStart);
                    //write a blank character
                    Console.Write(' ');
                }
            }

            //update the x start pos to take into account the width of the second character and add some spacing
            xStart += m_CharGeomList[m_iFirstChar].Dimensions.X + spacing;

            //for each row in the third character
            for (int y = 0; y < m_CharGeomList[m_iSecondChar].Geometry.Count; y++)
            {
                //for each character in the iterated row
                for (int x = 0; x < m_CharGeomList[m_iFirstChar].Geometry[y].Count; x++)
                {
                    //set the cursor pos to the xstart pos and take into account the iterated character pos, so if it is drawing the third character along the fourth row this needs to be added to start positions for the x and y
                    Console.SetCursorPosition(x + xStart, y + yStart);
                    //write a blank character
                    Console.Write(' ');
                }
            }
        }
    }
}