﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopterGame.Interfaces
{
    //an interface that listens for game message, class inherits IListener then registers with game class then when a game message is recived message is called in calss that inherited it
    interface IListener
    {
        //a parent class is required to define this function; if the class registers itself with the game class it will recive game messages through this function when one is dispatched
        void Message(CGame.GameMessages msg);
    }
}
