﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CopterGame.Interfaces;

namespace CopterGame.Entities
{
    //this is the player class it is designed to be controlled via key presses but functions mostly like any other entity
    class CPlayer : CEntity, Interfaces.IKeyListener
    {
        //all instances of the box class can share one instance of the geometry so this saves alot of memory and performance
        private static CGeometry sm_HeliGeom = new CGeometry(CGeometry.GetGeomDir() + "Helicopter.geom");
        //public constructor as it will be called by the entity manager
        public CPlayer()
        {
            //we want to monitor key presses so we need to register this class with the key manager
            CKeyManager.Instance.RegisterListener(this);
            //set the geometry variable (inhertied from CEntity) to that static instance of the geometry
            this.m_Geometry = sm_HeliGeom;
            //set that bDraw flag to true as we want it to be drawn
            this.m_bDraw = true;
            //set the postion of the player to be in the center at the left side of the screen
            this.m_2dPos = new Math.Vector2D(CCanvas.Instance.DrawXStart + 2, CCanvas.Instance.DrawYStart + (CCanvas.Instance.Height / 2));
            //intialize the previous pos to the start pos
            this.m_2dPrevPos = this.m_2dPos;
            //set the dimensions to match that in the geometry
            this.m_2dDimensions = this.m_Geometry.Dimensions;
            //set the is player flag to true as this is the player entity
            this.m_bIsPlayer = true;

            return;
        }
        //function that is called by key manager on a key event
        public void KeyEvent(ConsoleKeyInfo cki)
        {
            //switch statement to handle specific key press
            switch (cki.Key)
            {
                default:
                    break;

                //only commenting one as they all do pretty much the same thing
                //on key press in this case right arrow
                case ConsoleKey.RightArrow:
                    {
                        //update the previous position
                        m_2dPrevPos = m_2dPos;
                        //move the players position one unit in the positive x
                        m_2dPos = m_2dPos.Add(new Math.Vector2D(1, 0));
                        break;
                    }

                case ConsoleKey.LeftArrow:
                    {
                        m_2dPrevPos = m_2dPos;
                        m_2dPos = m_2dPos.Add(new Math.Vector2D(-1, 0));
                        break;
                    }

                case ConsoleKey.UpArrow:
                    {
                        m_2dPrevPos = m_2dPos;
                        m_2dPos = m_2dPos.Add(new Math.Vector2D(0, -1));
                        break;
                    }

                case ConsoleKey.DownArrow:
                    {
                        m_2dPrevPos = m_2dPos;
                        m_2dPos = m_2dPos.Add(new Math.Vector2D(0, 1));
                        break;
                    }
            }

            return;
        }

        //called per frame
        public override void Update()
        {
            //if the player isnt marked as alive then post the game over message as the game needs to end
            //no need to destroy this entity as they will all be destroyed at the end
            if (!this.m_bIsAlive)
            {
                CGame.Instance.PostMessage(CGame.GameMessages.GAME_OVER);
            }

            base.Update();
        }
        //called when a collision event arises with this object
        public override void CollisionEvent(IPhysicalable obj)
        {
            //Check that the object collided with isnt a power up
            if(!CEntityManager.Instance.GetEntity(obj).IsPowerUp)
            {
                //the object wasnt a powerup so we cant ignore it and its killed the player ;)
                m_bIsAlive = false;
            }

            base.CollisionEvent(obj);
        }
        //called when this entity is destroyed
        public override void Destroy()
        {
            //remove the key listener
            CKeyManager.Instance.DeregisterListener(this);
            base.Destroy();
        }
    }
}
