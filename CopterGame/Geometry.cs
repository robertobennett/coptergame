﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CopterGame
{
    class CGeometry
    {
        //filepath where the geometry (and font) files are stored
        public static string GetGeomDir()
        {
            return AppDomain.CurrentDomain.BaseDirectory + "Geometry/";
        }
        //constructor contains the file to read the geometry from
        public CGeometry(string sFile)
        {
            //calls the load geometry function
            LoadGeometry(sFile);

            return;
        }

        //hide the standard constructor
        private CGeometry()
        {
            return;
        }

        //create a list of list of characters so you can have rows and columns
        private List<List<char>> m_Geometry = new List<List<char>>();
        //create a property to retrive the geometry
        public List<List<char>> Geometry
        {
            get
            {
                return m_Geometry;
            }
        }
        //create a vector to store the widtha and height of the geometry
        Math.Vector2D m_Dimensions = new Math.Vector2D(0, 0);
        //property to retrive the dimensions
        public Math.Vector2D Dimensions
        {
            get
            {
                return m_Dimensions;
            }
        }
        //function to access the file read it in and process the geometry
        public bool LoadGeometry(string sFile)
        {
            //check the file actually contains characters
            if (sFile.Count() != 0)
            {
                //create a var to track the length of the longest row to save the dimensions
                int iMaxChars = 0;
                //create the reader to read the file
                StreamReader reader = new StreamReader(sFile);
                //create a list of characters to store the row
                List<char> line = new List<char>();
                //peek at the next character if the value returned isnt -1 then stay in the loop as it marks end of file
                while (reader.Peek() >= 0)
                {
                    //...then read the character
                    char c = (char)reader.Read();
                    //check if the character read is an end of line character if it isnt...
                    if (c != '\n' && c != '\r')
                    {
                        //...add it to the line
                        line.Add(c);
                    }
                    //if the line isnt empty
                    else if (line.Count > 0)
                    {
                        //it is a new line so we need to check if that line was the longest and if it was update the max rows
                        if (line.Count > iMaxChars)
                            iMaxChars = line.Count;

                        //otherwise add the row to the list of rows
                        Geometry.Add(line);
                        //and clear the line
                        line = new List<char>();
                    }
                }

                //will be a line left over from algorithim if the last char of last line isnt \n
                if (line.Count > 0)
                    Geometry.Add(line);

                //now create the dimensions (maximum characters in a line, number of rows)
                m_Dimensions = new Math.Vector2D(iMaxChars, m_Geometry.Count);

                return true;
            }

            //file was empty so return false
           return false;
        }
    }
}
